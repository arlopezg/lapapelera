$(document).ready(function () {

    $("#signupForm").validate({
        rules: {
            "nom":
            {
                required: true
            },
            "ape":
            {
                required: true
            },
            "pword":
            {
                required: true
            },
            "pword_repeat":
            {
                required: true,
                equalTo: pword
            },
            "tlf":
            {
            },
            "edad":
            {
            },
        },
        messages: {
            "name-contact": {
                required: "Please, enter a name"
            },	
        },
        submitHandler: function (form) { // for demo
            alert('valid form submitted'); // for demo
            return false; // for demo
        }
    });

});