tinymce.init({
	selector: '#content',
	language: 'es',
	height: 200,
	plugins: [
		'autolink link image lists charmap preview hr pagebreak',
		'searchreplace wordcount visualblocks visualchars code fullscreen media nonbreaking',
		'save table contextmenu directionality emoticons paste textcolor',
	],
	toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview media fullpage | forecolor backcolor emoticons'
});