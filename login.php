<!DOCTYPE html>
<head>
	<meta charset="UTF-8">
	<title>La Papelera Tiene Hambre</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/index.css"/>
	<link rel="stylesheet" type="text/css" href="css/fonts.css">
	<link rel="stylesheet" type="text/css" href="css/footer.css">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="shortcut icon" type="image/png" href="img/ico.png"/>
</head>
<body>
	<?php
		session_start();
		if(!empty($_SESSION['username'])){
			"header('Location: login.php');
			 exit();";
		}
		//else
	?>
	<!--form-->	
	<div class="main container">
		<section class="row">
			<div class="form-signin col-md-8 col-md-offset-2 clearfix">
				<a href="index.php">
					<img style="display: block; margin-left: auto; margin-right: auto; padding: 10px;" src="img/ico.png" alt="LPTH">
				</a>
				<form id="formReg" action="session.php" method="post">
					<label for="usernameusername" class="sr-only">Nombre de Usuario</label>
					<input type="text" id="username" name="username" class="form-control" placeholder="Nombre de Usuario" required autofocus maxlength="30" value="" autofocus>
					<br>
					<label for="pword" class="sr-only">Contrase&ntia</label>
					<input type="password" id="pword" name="pword" class="form-control" placeholder=" Contrase&ntilde;a" required maxlength="30" value="">
					<br>
					<button class="btn btn-md btn-success btn-block" id="btnLogin" name="btnLogin" type="submit" value="Iniciar sesi&oacute;n">Iniciar Sesión</button>
				</form>
				<span style="display: block; font-family: Montserrat; font-size: 24px; font-weight: 300; color: #3D3D3B; text-align: center; letter-spacing: -2px;">¿ Nuevo por aqu&iacute; ?
				</span>
				<form id="formReg" action="form-signin col-md-8 col-lg-9 clearfix offset-md-2">
					<button class="btn btn-md btn-warning btn-block">
						<a href="signup.php" style="text-decorations:none">Regístrate</a>
					</button>
				</form>
			</div>
		</section>
	</div>
	<?php include "pieces/footer.php" ?>
	<script src="js/jquery-3.1.1.min.js"></script>
	<script src="js/bootstrap.js"></script>
</body>
</html>