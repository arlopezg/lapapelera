
	<?php
		//POR HACER: si no se es Admin/Lider, no postea 
		$conn = mysqli_connect('localhost','root','','lapapelera');
		mysqli_select_db($conn,'lapapelera');
		mysqli_set_charset($conn,"utf8");

		//crear act
		if(!empty($_SESSION['username'])){//si el usuario NO está desconectado...
		$query  = "SELECT * FROM usuario WHERE username = '".$_SESSION['username']."'";
		$result = mysqli_query($conn,$query);
		while($row = mysqli_fetch_array($result)){
				if($row['userType']=="administrador"||$row['userType']=="lider"){
					echo "<a href='activity.php' class='btn btn-default btn-block btn-lg'>Crear Actividad</span></a>";
				}
				else{
					echo "No tiene permisos para publicar";
				}
			}//while
	}//while

	//listar noticias
	$query = "SELECT * FROM actividad ORDER BY idActividad DESC";
	$result = mysqli_query($conn,$query);

	while($row = mysqli_fetch_array($result)){
		//count comments
		$busqueda = "SELECT COUNT(*) AS total FROM comentario where idActividad = ".$row['idActividad'];
		$resultado = mysqli_query($conn,$busqueda);
		$values = mysqli_fetch_assoc($resultado); 
		$numComments = $values['total'];
		// strip tags to avoid breaking any html
		$strippedContent = $row['descripcion'];
		$strippedContent = strip_tags($strippedContent);
		//Cortar contenido a 300 chars, mostrar "Leer Más"
		if (strlen($strippedContent)>110){
		    // truncate string
		    $stringCut = substr($strippedContent,0,110);
		    // make sure it ends in a word so assassinate doesn't become ass...
		    $strippedContent = substr($stringCut,0,strrpos($stringCut,' '))."... <a href='getact.php?id=".$row['idActividad']."'>Leer Más.</a>"; 
		}//if
		//Mostrar c/Noticia
		$id = $row['idActividad'];
		echo "<article class='post page-header clearfix'>";
		/*Imagen*/
		echo "<a href='getact.php?id=".$row['idActividad']."' class='thumb pull-left'><img class='img-thumbnail' src='".$row['imagen']."'' alt='Imagen de Actividad'></a>";
		/*Titulo*/
		echo "<h2 class='post-title'><a href='getact.php?id=".$row['idActividad']."'>".$row['titulo']."</a></h2>";
		/*Publicado [fecha] por [autor]*/
		echo "<p>Publicado el ".$row['fecha']."</span> por <span><a href='$'>".$row['autor']."</a></p>";
		echo "<p>Fecha Inicio: ".$row['startDate']."</p>";
		echo "<p>Fecha Fin: ".$row['endDate']."</p>";
		echo "<p>Realizado en la ";//...
		if($row['idCircuito']==1)	echo "Zona Norte</p>";
		else 						echo "Zona Sur</p>";
		echo"<br>";
		/*Contenido*/
		echo "<p class='post-content text-justify'>".$strippedContent."</p>";
		echo "<div class='container-buttons'>";
		echo "<a href='getact.php?id=".$row['idActividad']."' class='btn btn-md btn-primary'>Leer Mas</a>";
		echo "<a href='getact.php?id=".$row['idActividad']."#comments' class='btn btn-md btn-success'>Comentarios <span class='badge'>".$numComments."</span></a>";
		//botones modif. elim.
		if(!empty($_SESSION['username'])){//si el usuario NO está desconectado...
			$userTypeQuery  = "SELECT * FROM usuario WHERE username = '".$_SESSION['username']."'";
			$userTypeResult = mysqli_query($conn,$userTypeQuery);
			while($row = mysqli_fetch_array($userTypeResult)){
				if($row['userType']=="administrador"){
				echo "<form action='deleteRecord.php' method='post' style='border:none;margin-right:0;margin-top:-2%' class='pull-right'>";
				echo "<a href='activity.php?id=$id' class='btn btn-md btn-warning pull-right'>Modificar</span></a>";
				echo "<input type='text' hidden name='idDelete' id='idDelete' value=".$id.">";
				echo "<input type='text' hidden name='recordType' id='recordType' value='actividad'>";
				echo "<input type='submit' class='btn btn-md btn-danger pull-right' value='Eliminar'></form>";
				}
				else echo "No tiene permisos para publicar";
			}//while
		}//if
		echo "</div>";
		echo "</article>";
	}//if
?>
