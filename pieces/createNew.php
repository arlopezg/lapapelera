<?php
	$username = $_SESSION['username'];
	date_default_timezone_get();
	$current = date('Y-m-d');
	echo "
		<fieldset>
			<form class='form' action='newsManager.php' method='post' enctype='multipart/form-data'>
				<h1>Gestor de Contenido <small>Crear Noticia</small></h1>
				<div class='form-group'><!--title-->
					<label for='title'><b>Título</b></label>
					<input class='form-control' type='text' name='title' id='title' value=''required>
				</div>

				<div class='form-group'>
					<label for='title'><b>Cabecera</b></label>
					<input type='file' class='form-control' name='imagen' id='imagen' size='30000' value=''/>
				</div>

				<div class='form-group'><!--content-->
					<label for='content'><b>Contenido</b></label>
					<textarea class='form-control' name='content' id='content' value='' rows='6' cols='100' maxlength='5000' placeholder=' Ingresa el contenido de la Noticia. Permite el ingreso de código HTML.'></textarea>
				</div>

				<div class='form-group'><!--autor-->
					<label for='author'><b>Autor</b></label>
					<input type='text' class='form-control' name='author' id='author' required minlength='1' disabled value=$username>
				</div>

				<div class='form-group'><!--date-->
					<label for='fecha'><b>Fecha</b></label>
					<input type='date' class='form-control' name='fecha' id='fecha' disabled value=$current>
				</div>

				<div class='form-group'>
					<input  type='submit' class='btn btn-default btn-lg btn-block' name='btnPublicar' value='Publicar Noticia'>
				</div>
			</form>
		</fieldset>";
?>