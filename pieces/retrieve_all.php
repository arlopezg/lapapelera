<?php
$conn = mysqli_connect('localhost','root','','lapapelera');
mysqli_select_db($conn,'lapapelera');
mysqli_set_charset($conn,"utf8");

//listar noticias
$query 	= 	"(SELECT titulo,fecha,autor,imagen,contenido,idNoticia FROM noticia) 
			UNION 
			(SELECT titulo,fecha,autor,imagen,descripcion,idActividad FROM actividad)
			ORDER BY fecha LIMIT 10";
$result = mysqli_query($conn,$query);

while($row = mysqli_fetch_array($result)){
	//count comments
	$busqueda = "SELECT COUNT(*) AS total FROM comentario where idNoticia= ".$row['idNoticia'];
	$resultado = mysqli_query($conn,$busqueda);
	$values = mysqli_fetch_assoc($resultado); 
	$numComments = $values['total'];
	// strip tags to avoid breaking any html
	$strippedContent = $row['contenido'];
	$strippedContent = strip_tags($strippedContent);
	//Cortar contenido a 300 chars, mostrar "Leer Más"
	if (strlen($strippedContent)>110){
	    // truncate string
	    $stringCut = substr($strippedContent,0,110);
	    // make sure it ends in a word so assassinate doesn't become ass...
	    $strippedContent = substr($stringCut,0,strrpos($stringCut,' '))."... <a href='getnew.php?id=".$row['idNoticia']."'>Leer Más.</a>"; 
	}//if
	//Mostrar c/Noticia
	echo "<article class='post page-header clearfix'>";
	/*Imagen*/
	echo "<a href='getnew.php?id=".$row['idNoticia']."' class='thumb pull-left'><img class='img-thumbnail' src='".$row['imagen']."' alt='Imagen de Noticia'></a>";
	/*Titulo*/
	echo "<h2 class='post-title'><a href='getnew.php?id=".$row['idNoticia']."'>".$row['titulo']."</a></h2>";
	/*Publicado [fecha] por [autor]*/
	echo "<p><span class='post-date'>Publicado el ".$row['fecha']."</span> por <span><a href='$'>".$row['autor']."</a></span></p>";
	/*Contenido*/
	echo "<p class='post-content text-justify'>".$strippedContent."</p>";
	echo"<br>";
	echo"<br>";
	echo "<div class='container-buttons'>";
	echo "<a href='getnew.php?id=".$row['idNoticia']."' class='btn btn-md btn-primary'>Leer Mas</a>";
	echo "<a href='getnew.php?id=".$row['idNoticia']."#comments' class='btn btn-md btn-success'>Comentarios <span class='badge'>".$numComments."</span></a>";
	echo "</div>";
	echo "</article>";
}//if
?>