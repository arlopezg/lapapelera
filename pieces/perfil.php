<?php
	if(empty($_SESSION['username'])){
		header("Location: login.php");
		exit;
		/*SI TE ENVÍA A login.php ES PORQUE
		NO ESTÁS LOGUEADO, DUMBASS!*/
	}else{
		echo '<h2><center>MI PERFIL</center></h2><hr style="border-top: dotted 1px;width:30%;color:#c0c0c0">';	
		$conn = mysqli_connect("localhost","root","","lapapelera");
		mysqli_select_db($conn,'lapapelera');
		mysqli_set_charset($conn,"utf8");

		$result = mysqli_query($conn,"SELECT * FROM usuario WHERE username='".$_SESSION['username']."'") or die(mysql_error());
		if(mysqli_num_rows($result)==1) {
		    $row = mysqli_fetch_array($result);
		    //Results can be accessed like $row['username'] and $row['Email']
		}

	echo "<form class='col-xs-8 col-md-10 col-lg-8' id='signupForm' action='perfilManager.php' method='post'>
			<div class='form-group'>
				<label for='email'>Correo Electr&oacute;nico</label>
				<input class='form-control' type='email' disabled name='email' id='email' required minlength='6' maxlength='30' value='".$row['email']."'>
			</div>

			<div class='form-group'>
				<label for='username'>Nombre de Usuario</label>
				<input class='form-control' disabled type='text' name='username' id='username' required maxlength='30' data-validation='length' data-validation-length='min4' data-validation-length='max10' value='".$row['username']."'>
			</div>

			<div class='form-group'>
				<label for='pword'>Contrase&ntilde;a</label>
				<input class='form-control' type='password' name='pword' id='pword' required minlength='6' maxlength='30'>
			</div>

			<div class='form-group'>
				<label for='pword_repeat'>Repite tu Contrase&ntilde;a</label>
				<input class='form-control' type='password' name='pword_repeat' id='pword_repeat' required minlength='6' maxlength='30'>
			</div>

			<div class='form-group'>
				<label for='nom'>Nombre</label>
				<input class='form-control' type='text' name='nom' id='nom'minlength='2' maxlength='30' value='".$row['nombre']."'>
			</div>

			<div class='form-group'>
				<label for='ape'>Apellido</label>
				<input class='form-control' type='text' name='ape' id='ape' required minlength='2' maxlength='30' value='".$row['apellido']."'>
			</div>

			<div class='form-group'>
				<label for='tlf'>Número Telef&oacute;nico</label>
				<div class='col-md-12 col-sm-12'>
					<input class='form-control' type='number' name='tlf' id='tlf' min='11111111111' max='99999999999' maxlength='11' value='".$row['telef']."'>
				</div>
			</div>
			<div class='form-group'>
				<label for='edad'>Edad</label>
				<input class='form-control' type='number' name='edad' id='edad' min='1' max='99' value='".$row['edad']."'>
			</div>

			<div class='form-group'>
				<label for='direccion'>Direccion de Domicilio</label>
				<input type='text' class='form-control' name='direccion' id='direccion' rows='5' maxlength='100' value='".$row['direccion']."'>
			</div>

			<div class='form-group'>
				<input class='btn btn-warning btn-block btn-lg' type='submit' id='btnRegistro' name='btnRegistro' value='Guardar cambios'>
			</div>
		</form>";
	}
?>