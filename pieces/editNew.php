<?php
	$conn = mysqli_connect('localhost','root','','lapapelera');
	mysqli_select_db($conn,'lapapelera');
	mysqli_set_charset($conn,"utf8");

	$id = $_REQUEST['id'];
	$query = "SELECT * FROM noticia WHERE idNoticia=$id";
	$result = mysqli_query($conn,$query);

	$username = $_SESSION['username'];
	date_default_timezone_get();
	$current = date('Y-m-d');
	while($row = mysqli_fetch_array($result)){
	$titulo = $row['titulo'];
	$imagen = $row['imagen'];
	$contenido = $row['contenido'];
	$autor = $row['autor'];
	$fecha = $row['fecha'];
	echo "
		<fieldset>
			<form class='form' action='newsManager.php' method='post' enctype='multipart/form-data'>
				<h1>Gestor de Contenido <small>Modificar Noticia (#$id)</small></h1>
				<div class='form-group'><!--title-->
					<label for='title'><b>Título</b></label>
					<input class='form-control' type='text' name='title' id='title' value='$titulo'>
					<input type='text' name='idNoticia' id='idNoticia' value='$id' hidden>
				</div>

				<div class='form-group'>
					<label for='title'><b>Cabecera</b></label>
					<input type='file' class='form-control' name='imagen' id='imagen' size='30000' value=''/>
				</div>

				<div class='form-group'><!--content-->
					<label for='content'><b>Contenido</b></label>
					<textarea class='form-control' name='content' id='content' value='' rows='6' cols='100' maxlength='5000' placeholder=' Ingresa el contenido de la Noticia. Permite el ingreso de código HTML.'>$contenido</textarea>
				</div>

				<div class='form-group'><!--autor-->
					<label for='author'><b>Autor</b></label>
					<input type='text' class='form-control' name='author' id='author' required minlength='1' disabled value=$username>
				</div>

				<div class='form-group'><!--date-->
					<label for='fecha'><b>Fecha</b></label>
					<input type='date' class='form-control' name='fecha' id='fecha' disabled value=$current>
				</div>

				<div class='form-group'>
					<input class='btn btn-default btn-lg btn-block' type='submit' name='btnPublicar' value='Modificar Noticia'>
				</div>
			</form>
		</fieldset>";
	}/*while*/
?>