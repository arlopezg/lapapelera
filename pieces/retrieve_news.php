<?php
$conn = mysqli_connect('localhost','root','','lapapelera');
mysqli_select_db($conn,'lapapelera');
mysqli_set_charset($conn,"utf8");
//crear act
if(!empty($_SESSION['username'])){//si el usuario NO está desconectado...
	$userTypeQuery  = "SELECT * FROM usuario WHERE username = '".$_SESSION['username']."'";
	$userType = mysqli_query($conn,$userTypeQuery);
	while($row = mysqli_fetch_array($userType)){
			if($row['userType']=="administrador")
				echo "<a href='news.php' class='btn btn-default btn-block btn-lg'>Crear Noticia</span></a>";
		}//while
}//if

//listar noticias
$query = "SELECT * FROM noticia ORDER BY idNoticia DESC";
$result = mysqli_query($conn,$query);

while($row = mysqli_fetch_array($result)){
	//count comments
	$busqueda = "SELECT COUNT(*) AS total FROM comentario where idNoticia= ".$row['idNoticia'];
	$resultado = mysqli_query($conn,$busqueda);
	$values = mysqli_fetch_assoc($resultado); 
	$numComments = $values['total'];
	// strip tags to avoid breaking any html
	$strippedContent = $row['contenido'];
	$strippedContent = strip_tags($strippedContent);
	//Cortar contenido a 300 chars, mostrar "Leer Más"
	if (strlen($strippedContent)>110){
	    // truncate string
	    $stringCut = substr($strippedContent,0,110);
	    // make sure it ends in a word so assassinate doesn't become ass...
	    $strippedContent = substr($stringCut,0,strrpos($stringCut,' '))."... <a href='getnew.php?id=".$row['idNoticia']."'>Leer Más.</a>"; 
	}//if
	//Mostrar c/Noticia
	$id = $row['idNoticia'];
	echo "<article class='post page-header clearfix'>";
	/*Imagen*/
	echo "<a href='getnew.php?id=".$row['idNoticia']."' class='thumb pull-left'><img class='img-thumbnail' src='".$row['imagen']."' alt='Imagen de Noticia'></a>";
	/*Titulo*/
	echo "<h2 class='post-title'><a href='getnew.php?id=".$row['idNoticia']."'>".$row['titulo']."</a></h2>";
	/*Publicado [fecha] por [autor]*/
	echo "<p><span class='post-date'>Publicado el ".$row['fecha']."</span> por <span><a href='$'>".$row['autor']."</a></span></p>";
	/*Contenido*/
	echo "<p class='post-content text-justify'>".$strippedContent."</p>";
	echo"<br>";
	echo"<br>";
	echo "<div class='container-buttons'>";
	echo "<a href='getnew.php?id=".$row['idNoticia']."' class='btn btn-md btn-primary'>Leer Mas</a>";
	echo "<a href='getnew.php?id=".$row['idNoticia']."#comments' class='btn btn-md btn-success'>Comentarios <span class='badge'>".$numComments."</span></a>";
	//botones modif. elim.
	if(!empty($_SESSION['username'])){//si el usuario NO está desconectado...
		$userTypeQuery  = "SELECT * FROM usuario WHERE username = '".$_SESSION['username']."'";
		$userTypeResult = mysqli_query($conn,$userTypeQuery);
		while($row = mysqli_fetch_array($userTypeResult)){
			if($row['userType']=="administrador"){
			echo "<form action='deleteRecord.php' method='post' style='border:none;margin-right:0;margin-top:-2%' class='pull-right'>";
			echo "<a href='news.php?id=$id' class='btn btn-md btn-warning pull-right'>Modificar</span></a>";
			echo "<input type='text' hidden name='idDelete' id='idDelete' value=".$id.">";
			echo "<input type='text' hidden name='recordType' id='recordType' value='noticia'>";
			echo "<input type='submit' class='btn btn-md btn-danger pull-right' value='Eliminar'></form>";
			}
		}//while
	}//if
	echo "</div>";
	echo "</article>";
}//if
?>