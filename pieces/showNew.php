<?php
	$username = $_SESSION['username'];
	date_default_timezone_get();
	$current = date('Y-m-d');
	echo "
		<fieldset>
			<form action='newsManager.php' method='post' enctype='multipart/form-data'>
			<legend>MANEJADOR DE NOTICIAS - CREAR NOTICIA</legend>
				<table>
					<tr><!--TitleRow-->
						<td>
							<label for='title'><b>Título</b></label>
						</td>
						<td>
							<input type='text' name='title' id='title' value='' maxlength='50' placeholder=' título de la noticia'required minlength='2' autofocus>
						</td>
					</tr>
					<tr><!--image-->
						<td>
							<label for='title'><b>Cabecera</b></label>
						</td>
						<td>
							<input type='file' name='imagen' id='imagen' size='30000' value=''/>
						</td>
					</tr>
					<tr><!--ContentRow-->
						<td>
							<label for='content'><b>Contenido</b></label>
						</td>
						<td>
							<textarea name='content' id='content' value='' rows='6' cols='100' maxlength='5000' placeholder=' Ingresa el contenido de la Noticia. Permite el ingreso de código HTML.' required></textarea>
						</td>
					</tr>
					<tr><!--AuthorRow-->
						<td>
							<label for='author'><b>Autor</b></label>
						</td>
						<td>";
							if(empty($username)){
								header('Location: login.php');
								exit();
								/*SI TE ENVÍA A login.php ES PORQUE
								NO ESTÁS LOGUEADO, DUMBASS!*/
							}else{
								echo "<input type='text' name='author' id='author' required minlength='1' disabled value=$username>";
							}
						echo "
						</td>
					</tr>
					</tr>
					<tr><!--dateRow-->
						<td>
							<label for='fecha'><b>Fecha</b></label>
						</td>
						<td>
							<input type='date' name='fecha' id='fecha' disabled value=$current>
						</td>
					</tr>
				</table>
				<hr>
				<!--enviar/reset-->
				<input class='btn btn-primary btn-block btn-lg' type='submit' id='btnPublicar' name='btnPublicar' value='Publicar Noticia'>
			</form>	
		</fieldset>";
?>