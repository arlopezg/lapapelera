<!DOCTYPE html>

<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>La Papelera Tiene Hambre</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/index.css">
	<link rel="stylesheet" type="text/css" href="css/signup.css">
	<link rel="stylesheet" type="text/css" href="css/fonts.css">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="shortcut icon" type="image/png" href="img/ico.png"/>
	<script src="js/jquery-3.1.1.min.js"></script>
	<script src="js/jquery.validate.js"></script>
	<script src="js/messages_es.js"></script>
	<script src="js/bootstrap.js"></script>
</head>
<body>
	<header>
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-1">
						<span class="sr-only">Menu</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a href="index.php" class="navbar-brand"><img src="img/brandicon.png" alt="LPTH" id="brandicon"></a>
				</div>
				<div class="collapse navbar-collapse" id="navbar-1"> 
					<ul class="nav navbar-nav lpth-navbar">
						<li class="active"><a href="index.php">INICIO</a></li>
						<li class=""><a href="retrieve_news.php">NOTICIAS</a></li>
						<li class=""><a href="activityRetrieve.php">ACTIVIDADES</a></li>
						<li class=""><a href="circuit_descrip.php">CIRCUITOS</a></li>
						<li class=""><a href="glossary.php">GLOSARIO</a></li>
						<li class=""><a href="contact.php">CONTACTO</a></li>
						<li><a>
							<?php
								session_start();
								if(empty($_SESSION['username'])){
									echo "<li><a href='login.php'>[Iniciar Sesión]</a></li>";
								}else{
								echo "
								<div class='btn-group hidden-sm hidden-xs pull-right'>
									</button>
									<ul class='dropdown-menu'>
									    	<li><a href='#'>Perfil</a></li>
								    		<li><a href='#'>Configuraci&oacute;n</a></li>
								    		<li role='separator' class='divider'></li>
								    		<li><a href='#'>Cerrar Sesi&oacute;n</a></li>
								  	</ul>
								</div>
								<div class='pull-right'>
									<li class='login.php'><a href='perfil.php'>[Iniciaste Sesión como ".$_SESSION['username']."]
										<span class='glyphicon glyphicon-user' data-toggle='tooltip' data-placement='bottom' title='Perfil y Configuraci&oacute;n'></span> </a></li>
									</p>
								</div>";
								}
								//else
							?>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<div class="header">
			<a href="index.php"><img src="img/header.png" alt="LPTH"></a>
		</div>
	</header>
	<!--form-->
	<!--MainContent-->
	<section class="main container">
		<div class="row">
			<section class="col-xs-12 col-sm-12 col-md-8 col-lg-9">
				<form class="" id="signupForm" action="signupManager.php" method="post">
					<!--email-->
					<div class="form-group">
						<label for="email">Correo electr&oacute;nico</label>
						<input class="form-control" type="email" name="email" id="email" required minlength="6" maxlength="30">
					</div>
					<!--username-->
					<div class="form-group">
						<label for="username">Nombre de usuario</label>
						<input class="form-control" type="text" name="username" id="username" required maxlength="30" data-validation="length" data-validation-length="min4" data-validation-length="max10">
					</div>
					<!--pword-->
					<div class="form-group">
						<label for="pword">Contrase&ntilde;a</label>
						<input class="form-control" type="password" name="pword" id="pword" required minlength="6" maxlength="30">
					</div>
					<!-- valid pword-->
					<div class="form-group">
						<label for="pword">Repite tu contrase&ntilde;a</label>
						<input class="form-control" type="password" name="pword_repeat" id="pword_repeat" required minlength="6" maxlength="30">
					</div>
					<!--nom-->
					<div class="form-group">
						<label for="nom">Nombre</label>
						<input class="form-control" type="text" name="nom" id="nom"minlength="2" maxlength="30">
					</div>
					<!--ape-->
					<div class="form-group">
						<label for="ape">Apellido</label>
						<input class="form-control" type="text" name="ape" id="ape" required minlength="2" maxlength="30">
					</div>
					<!--idCircuito-->
					<label for="idCircuito">Tu Circuito</label>
					<div class="form-group">
						<select class="form-control"name='idCircuito' id='idCircuito' required> 
							<option value=''>Selecciona un Circuito</option>
							<option value='1'>Zona Norte</option>
							<option value='2'>Zona Sur</option>
						</select>
					</div>
					<!--tlf(prefix)-->
					<label for="tlf_prefix">Operadora y Número Telef&oacute;nico</label>
					<div class="form-group">
						<div class="col-md-4">
							<select class="form-control" name="tlf_prefix" id="tlf_prefix">
								<option value="">Operadora</option>
								<option value="0412">0412</option>
								<option value="0414">0414</option>
								<option value="0424">0424</option>
								<option value="0416">0416</option>
								<option value="0426">0426</option>
							</select>
						</div>
						<div class="col-md-8">
							<input class="form-control" type="number" name="tlf" id="tlf" min="1111111" max="9999999" maxlength="7">
						</div>
					</div>
					<div class="form-group">
						<label for="edad">Edad</label>
						<input class="form-control" type="number" name="edad" id="edad" min="1" max="99">
					</div>
					<!--direccion-->
					<div class="form-group">
						<label for="direccion">Direccion de Domicilio</label>
						<textarea class="form-control" name="direccion" id="direccion" rows="5" maxlength="100"></textarea>
					</div>
					<!--checkbox-->
					<div class="form-group">
						<div class="checkbox">
							<label for="terminos">
								<input type="checkbox" name="terminos" id="terminos" required>
								Acepto los <a href="">t&eacute;rminos y condiciones
							</label>
						</div>
					</div>
					<!--enviar/reset-->
					<div class="form-group">
						<input class="btn btn-warning btn-block btn-lg" type="submit" id="btnRegistro" name="btnRegistro" value="¡Reg&iacute;strate!">
					</div>
				</form>
			</section> 
			<aside class="col-md-4 col-lg-3 hidden-xs hidden-sm pull-right">
				<h4>Usuarios</h4>
				<div class="list-group">
					<?php
					if(empty($_SESSION['username'])){
						echo "<a class='list-group-item' href='login.php' class='list-group-item'>Iniciar Sesion</a>";
						echo "<a class='list-group-item' href='signup.php' class='list-group-item'>Crear Usuario</a>";
					}else{
						echo "<a href='perfil.php' class='list-group-item'>Mi Perfil</a>";
						echo "<a href='reportes.php' class='list-group-item'>Estadísticas</a>";
						echo "<a href='destroy.php' class='list-group-item'>Salir</a>";
					}
					?>
				</div>
				<h4>Actividades</h4>
				<div class="list-group">
					<a class="list-group-item" href="#">Más populares</a>
					<a class="list-group-item" href="#">En tu zona</a>
					<a class="list-group-item" href="activityRetrieve.php">Más recientes</a>
					<br>
					<a class="list-group-item" href="zonanorte.php">Zona Norte</a>
					<a class="list-group-item" href="zonasur.php">Zona Sur</a>
				</div>
				<h4>Contacto</h4>
				<h4>Soporte Telefónico</h4>
				<p>(0261) 7778698<p/>
				<p>(0212) 7546939</p>
				<h4>Correo Electrónico</h4>
				<p>soporte@lapapelera.com</p>
			</aside>
		</div>
	</section>
	<?php include "pieces/footer.php" ?>
</body>
</html>