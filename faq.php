<!DOCTYPE html>
<head>
	<meta charset="UTF-8">
	<title>La Papelera Tiene Hambre</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/index.css"/>
	<link rel="stylesheet" type="text/css" href="css/fonts.css">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="shortcut icon" type="image/png" href="img/ico.png"/>
</head>
<body>
	<!--Navbar-->
	<header>
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-1">
						<span class="sr-only">Menu</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a href="index.php" class="navbar-brand"><img src="img/brandicon.png" alt="LPTH" id="brandicon"></a>
				</div>
				<div class="collapse navbar-collapse" id="navbar-1"> 
					<ul class="nav navbar-nav lpth-navbar">
						<li class=""><a href="index.php">INICIO</a></li>
						<li class=""><a href="retrieve_news.php">NOTICIAS</a></li>
						<li class=""><a href="activityRetrieve.php">ACTIVIDADES</a></li>
						<li class=""><a href="circuit_descrip.php">CIRCUITOS</a></li>
						<li class=""><a href="glossary.php">GLOSARIO</a></li>
						<li class=""><a href="contact.php">CONTACTO</a></li>
						<li><a>
							<?php
								session_start();
								if(empty($_SESSION['username'])){
									echo "<li><a href='login.php'>[Iniciar Sesión]</a></li>";
								}else{
								echo "
								<div class='btn-group hidden-sm hidden-xs pull-right'>
									</button>
									<ul class='dropdown-menu'>
									    	<li><a href='#'>Perfil</a></li>
								    		<li><a href='#'>Configuraci&oacute;n</a></li>
								    		<li role='separator' class='divider'></li>
								    		<li><a href='#'>Cerrar Sesi&oacute;n</a></li>
								  	</ul>
								</div>
								<div class='pull-right'>
									<li class='login.php'><a href='perfil.php'>[".$_SESSION['username']."]
										<span class='glyphicon glyphicon-user' data-toggle='tooltip' data-placement='bottom' title='Perfil y Configuraci&oacute;n'></span> </a></li>
									</p>
								</div>";
								}
								//else
							?>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<div class="header">
			<a href="index.php"><img src="img/header.png" alt="LPTH"></a>
		</div>
	</header>
	<!--form-->	
	<section class="courses">
		<article>
			<figure>
			<ul>
				<li>
					<h3>¿Lorem ipsum dolor sit amet?</h3>
					<ul><li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi qui aliquam tempore obcaecati atque doloribus, quae nam dolor minus culpa blanditiis. Perspiciatis accusantium odio, odit id obcaecati explicabo fugit consequuntur.</li></ul>
				</li>
				<br>
				<li>
					<h3>¿Incidunt doloribus, odit facere error qui, esse quasi quam quidem id ut adipisci? </h3>
					<ul><li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed ullam, nesciunt nobis debitis, blanditiis temporibus, nam vel laboriosam consequatur repudiandae facilis quibusdam? Laudantium praesentium maxime temporibus a odit tempore sunt.</li></ul>
				</li>
				<br>
				<li>
					<h3>¿Reiciendis ratione velit laboriosam at itaque consequatur quod ducimus aliquid suscipit incidunt sed minus?</h3>
					<ul><li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro rem, doloremque sapiente unde expedita aperiam recusandae eos ut voluptate autem commodi animi voluptates in provident repellat explicabo id quasi ducimus!</li></ul>
				</li>
				<br>
				<li>
					<h3>¿Quisquam dolores, modi dolor doloribus quam et reiciendis vitae voluptatum, delectus	?</h3>
					<ul><li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur inventore sit, eos unde reiciendis eum corporis magnam iusto, dolor sapiente a quis nam sequi debitis assumenda vel porro voluptatum rem.</li></ul>
				</li>
				<br>
				<li>
					<h3>¿Est eligendi ex facilis quibusdam totam nulla assumenda tenetur voluptatem, ullam repellat?</h3>
					<ul><li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad est facilis, pariatur a reprehenderit sapiente quam minima voluptate, ipsam laudantium dolorem harum, voluptatem, mollitia! Eos id voluptatum officia laboriosam ad?</li></ul>
				</li>
			</ul>
			</figure>
		</article>
	</section>
	<!--SiderBar-->
	<aside class="col-md-3 col-lg-3 hidden-xs hidden-sm pull-right">
		<h4>Registro</h4>
		<div class="list-group">
			<a class="list-group-item" href="login.php">Iniciar Sesion</a>
			<a class="list-group-item" href="signup.php">Crear Usuario</a>
		</div>

		<h4>Actividades</h4>
		<div class="list-group">
					<a class="list-group-item" href="#">Más populares</a>
					<a class="list-group-item" href="#">En tu zona</a>
					<a class="list-group-item" href="activityRetrieve.php">Más recientes</a>
					<br>
					<a class="list-group-item" href="zonanorte.php">Zona Norte</a>
					<a class="list-group-item" href="zonasur.php">Zona Sur</a>
		</div>
		<br>
		<h4>Contacto</h4>
		<h4>Soporte Telefónico</h4>
		<p>(0261) 7778698<p/>
		<p>(0212) 7546939</p>
		<h4>Correo Electrónico</h4>
		<p>soporte@lapapelera.com</p>
	</aside>
	<!--/SiderBar-->
	<?php include "pieces/footer.php" ?>
	<script src="js/jquery-3.1.1.min.js"></script>
	<script src="js/bootstrap.js"></script>
</body>
</html>