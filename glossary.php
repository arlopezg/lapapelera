<!DOCTYPE html>
<head>
	<meta charset="UTF-8">
	<title>La Papelera Tiene Hambre</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/index.css"/>
	<link rel="stylesheet" type="text/css" href="css/fonts.css">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="shortcut icon" type="image/png" href="img/ico.png"/>
</head>
<body>
	<!--Navbar-->
	<header>
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-1">
						<span class="sr-only">Menu</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a href="index.php" class="navbar-brand"><img src="img/brandicon.png" alt="LPTH" id="brandicon"></a>
				</div>
				<div class="collapse navbar-collapse" id="navbar-1"> 
					<ul class="nav navbar-nav lpth-navbar">
						<li class=""><a href="index.php">INICIO</a></li>
						<li class=""><a href="retrieve_news.php">NOTICIAS</a></li>
						<li class=""><a href="activityRetrieve.php">ACTIVIDADES</a></li>
						<li class=""><a href="circuit_descrip.php">CIRCUITOS</a></li>
						<li class="active"><a href="glossary.php">GLOSARIO</a></li>
						<li class=""><a href="contact.php">CONTACTO</a></li>
						<li><a>
							<?php
								session_start();
								if(empty($_SESSION['username'])){
									echo "<li><a href='login.php'>[Iniciar Sesión]</a></li>";
								}else{
								echo "
								<div class='btn-group hidden-sm hidden-xs pull-right'>
									</button>
									<ul class='dropdown-menu'>
									    	<li><a href='#'>Perfil</a></li>
								    		<li><a href='#'>Configuraci&oacute;n</a></li>
								    		<li role='separator' class='divider'></li>
								    		<li><a href='#'>Cerrar Sesi&oacute;n</a></li>
								  	</ul>
								</div>
								<div class='pull-right'>
									<li class='login.php'><a href='perfil.php'>[".$_SESSION['username']."]
										<span class='glyphicon glyphicon-user' data-toggle='tooltip' data-placement='bottom' title='Perfil y Configuraci&oacute;n'></span> </a></li>
									</p>
								</div>";
								}
								//else
							?>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<div class="header">
			<a href="index.php"><img src="img/header.png" alt="LPTH"></a>
		</div>
	</header>
	<!--FinNavbar-->
	<!--MainContent-->
	<section class="main container">
		<div class="row">
			<section class="col-md-9 col-lg-9">
				<article class="post page-header clearfix">
				<figure>
				<h2>Glosario de Términos</h2>
				<hr>
				<ul>
					<li><b>Basura:</b> se considera de forma genérica a los residuos sólidos sean urbanos, industriales, etc. Ver Residuos sólidos y Residuos sólidos urbanos.</li>
					<hr>
					<li><b>Calcín:</b> chatarra de vidrio fragmentado, acondicionado o no para su fundición.</li>
					<hr>
					<li><b>Clasificación de los residuos:</b> atendiendo al estado y al soporte en que se presentan, se clasifican en sólidos, líquidos y gaseosos. La referncia al soporte se debe a la existencia de numerosos residuos aparentemente de un tipo, pero que están integrados por varios (gaseosos formados por partículas sólidas y líquidas, líquidos con partículas sólidas, etc.) por lo que se detrmina que su estado es el que presenta el soporte principal del residuo (gaseoso en el primer ejemplo, líquido en el segundo). Ver Residuos sólidos.</li>
					<hr>
					<li><b>Compost o compuesto:</b> producto obtenido mediante el proceso de compostaje.</li>
					<hr>
					<li><b>Compostaje:</b> reciclaje completo de la materia orgánica mediante el cual ésta es sometida a fermentación controlada (aerobia) con el fin de obtener un producto estable, de características definidas y útil para la agricultura.</li>
					<hr>
					<li><b>Chatarra:</b> restos producidos durante la fabricación o consumo de un material o producto. Se aplica tanto a objetos usados, enteros o no, como a fragmentos resultantes de la fabricación de un producto. Se utiliza fundamentalmente para metales y también para vidrio.</li>
					<hr>
					<li><b>Escombros:</b> restos de derribos y de construcción de edificaciones, constituidos principalmente por tabiquería, cerámica, hormigón, hierros, madera, plásticos y otros, y tierras de excavación en las que se incluyen tierra vegetal y rocas del subsuelo.</li>
					<hr>
					<li><b>Granza de plástico de recuperación:</b> producto obtenido de reciclar plásticos usados y que equivale a los productos plásticos de primera transformación o "granza virgen". Normalmente se presenta en forma de fino "macarrón" troceado.</li>
					<hr>
					<li><b>Materia inerte:</b> vidrio (envases y plano), papel y cartón, tejidos (lana, trapos y ropa), metales (férricos y no férricos), plásticos, maderas, gomas, cueros, loza y cerámica, tierras, escorias, cenizas y otros. A pesar de que pueden fermentar el papel y cartón, así como la madera y en mucha menor medida ciertos tejidos naturales y el cuero, se consideran inertes por su gran estabilidad en comparación con la materia orgánica. Los plásticos son materia orgánica, pero no fermentable.</li>
					<hr>
					<li><b>Reciclaje:</b> proceso simple o complejo que sufre un material o producto para ser reincorporado a un ciclo de producción o de consumo, ya sea éste el mismo en que fue generado u otro diferente. La palabra "reciclado" es un adjetivo, el estado final de un material que ha sufrido el proceso de reciclaje. En términos de absoluta propiedad se podría considerar el reciclaje puro sólo cuando el producto material se reincorpora a su ciclo natural y primitivo:</b> materia orgánica que se incorpora al ciclo natural de la materia mediante el compostaje. Sin embargo y dado lo restrictivo de esta acepción pura, extendemos la definición del reciclaje a procesos más amplios. Según la complejidad del proceso que sufre el mateial o producto durante su reciclaje, se establecen dos tipos:</b> directo, primario o simple; e indirecto, secundario o complejo.</li>
					<hr>
					<li><b>Recogida selectiva:</b> recogida de residuos separados y presentados aisladamente por su productor.</li>
					<hr>
					<li><b>Recuperación:</b> sustracción de un residuo a su abandono definitivo. Un residuo recuperado pierde en este proceso su carácter de "material destinado a su abandono", por lo que deja de ser un residuo propiamente dicho, y mediante su nueva valoración adquiere el carácter de "materia prima secundaria".</li>
					<hr>
					<li><b>Rechazo:</b> resto producido al reciclar algo.</li>
					<hr>
					<li><b>Residuo:</b> todo material en estado sólido, líquido o gaseoso, ya sea aislado o mezclado con otros, resultante de un proceso de extracción de la Naturaleza, transformación, fabricación o consumo, que su poseedor decide abandonar.</li>
					<hr>
					<li><b>Residuos peligrosos:</b> sólidos, líquidos (más o menos espesos) y gases que contengan alguna(s) sustancia(s) que por su composición, presentación o posible mezcla o combinación puedan significar un peligro presente o futuro, directo o indirecto para la salud humana y el entorno.</li>
					<hr>
					<li><b>Residuos sólidos:</b> en función de la actividad en que son producidos, se clasifican en agropecuarios (agrícolas y ganaderos), forestales, mineros, industriales y urbanos. A excepción de los mineros, por sus características de localización, cantidades, composición, etc., los demás poseen numerosos aspectos comunes desde el punto de vista de la recuperación y reciclaje.</li>
					<hr>
					<li><b>Residuos sólidos urbanos (RSU):</b> son aquellos que se generan en los espacios urbanizados, como consecuencia de las actividades de consumo y gestión de actividades domésticas (viviendas), servicios (hostelería, hospitales, oficinas, mercados, etc.) y tráfico viario (papeleras y residuos viarios de pequeño y gran tamaño).</li>
					<hr>
					<li><b>Reutilizar:</b> volver a usar un producto o material varias veces sin "tratamiento", equivale a un "reciclaje directo". El relleno de envases retornables, la utilización de paleas ("pailets") de madera en el transporte, etc., son algunos ejemplos.</li>
					<hr>
					<li><b>TEP:</b> Abreviatura de "Tonelada equivalente de petróleo":</b> se utiliza como unidad energética y sirve para comparar la cantidad de energía que contiene un material como carbón, plástico, agua embalsada, etc. con la que contiene una tonelada de petróleo, es decir que el petróleo se considera como patrón de medida, la unidad. Un Tep = 11.678,8 Kwh.</li>
					<hr>
					<li><b>Tratamiento:</b> conjunto de operaciones por las que se alteran las propiedades físicas o químicas de los residuos.</li>
					<hr>
					<li><b>Triar o destriar:</b> seleccionar o separar diversos componentes de la basura normalmente de forma manual.</li>
					<hr>
					<li><b>Vertido:</b> deposición de los residuos en un espacio y condiciones determinadas. Según la rigurosidad de las condiciones y el espacio de vertido, en relación con la contaminación producida, se establecen los tres tipos siguientes.</li>
					<hr>
					<li><b>Vertido controlado:</b> acondicionamiento de los residuos en un espacio destinado al efecto, de forma que no produzcan alteraciones en el mismo, que puedan significar un peligro presente o futuro, directo o indirecto, para la salud humana ni el entorno.</li>
					<hr>
					<li><b>Vertido semicontrolado:</b> acondicionamiento de los residuos en un determinado espacio, que sólo evita de forma parcial la contaminación del entorno.</li>
					<hr>
					<li><b>Vertido incontrolado:</b> o salvaje de residuos sin acondicionar:</b> es aquel cuyos efectos contaminantes son desconocidos.</li>
				</ul>
			</figure>
		</article>
			</section> 
			<!--Sidebar-->
			<aside class="col-md-3 col-lg-3 hidden-xs hidden-sm pull-right">
				<h4>Usuarios</h4>
				<div class="list-group">
					<?php
					if(empty($_SESSION['username'])){
						echo "<a class='list-group-item' href='login.php' class='list-group-item'>Iniciar Sesion</a>";
						echo "<a class='list-group-item' href='signup.php' class='list-group-item'>Crear Usuario</a>";
					}else{
						echo "<a href='perfil.php' class='list-group-item'>Mi Perfil</a>";
						echo "<a href='reportes.php' class='list-group-item'>Estadísticas</a>";
						echo "<a href='destroy.php' class='list-group-item'>Salir</a>";
					}
					?>
				</div>
				<h4>Actividades</h4>
				<div class="list-group">
					<a class="list-group-item" href="#">Más populares</a>
					<a class="list-group-item" href="#">En tu zona</a>
					<a class="list-group-item" href="activityRetrieve.php">Más recientes</a>
					<br>
					<a class="list-group-item" href="zonanorte.php">Zona Norte</a>
					<a class="list-group-item" href="zonasur.php">Zona Sur</a>
				</div>
				<h4>Contacto</h4>
				<h4>Soporte Telefónico</h4>
				<p>(0261) 7778698<p/>
				<p>(0212) 7546939</p>
				<h4>Correo Electrónico</h4>
				<p>soporte@lapapelera.com</p>
			</aside>
			<!--FinSidebar-->
		</div>
	</section>
	<?php include "pieces/footer.php" ?>
	<script src="js/jquery-3.1.1.min.js"></script>
	<script src="js/bootstrap.js"></script>
</body>
</html>