<!DOCTYPE html>
<head>
	<meta charset="UTF-8">
	<title>La Papelera Tiene Hambre</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/index.css"/>
	<link rel="stylesheet" type="text/css" href="css/newsManager.css"/>
	<link rel="stylesheet" type="text/css" href="css/fonts.css">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="shortcut icon" type="image/png" href="img/ico.png"/>
	<script type="text/javascript" src='js/jquery-3.1.1.min.js'></script>
	<script type="text/javascript" src='js/tinymce/jquery.tinymce.min.js'></script>
	<script type="text/javascript" src='js/tinymce/tinymce.min.js'></script>
</head>
<body>
	<!--Navbar-->
	<header>
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-1">
						<span class="sr-only">Menu</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a href="index.php" class="navbar-brand"><img src="img/brandicon.png" alt="LPTH" id="brandicon"></a>
				</div>
				<div class="collapse navbar-collapse" id="navbar-1"> 
					<ul class="nav navbar-nav lpth-navbar">
						<li class=""><a href="index.php">INICIO</a></li>
						<li class=""><a href="retrieve_news.php">NOTICIAS</a></li>
						<li class="active"><a href="activityRetrieve.php">ACTIVIDADES</a></li>
						<li class=""><a href="circuit_descrip.php">CIRCUITOS</a></li>
						<li class=""><a href="glossary.php">GLOSARIO</a></li>
						<li class=""><a href="contact.php">CONTACTO</a></li>
						<li><a>
							<?php
								session_start();
								if(empty($_SESSION['username'])){
									echo "<li><a href='login.php'>[Iniciar Sesión]</a></li>";
								}else{
								echo "
								<div class='btn-group hidden-sm hidden-xs pull-right'>
									</button>
									<ul class='dropdown-menu'>
									    	<li><a href='#'>Perfil</a></li>
								    		<li><a href='#'>Configuraci&oacute;n</a></li>
								    		<li role='separator' class='divider'></li>
								    		<li><a href='#'>Cerrar Sesi&oacute;n</a></li>
								  	</ul>
								</div>
								<div class='pull-right'>
									<li class='login.php'><a href='perfil.php'>[".$_SESSION['username']."]
										<span class='glyphicon glyphicon-user' data-toggle='tooltip' data-placement='bottom' title='Perfil y Configuraci&oacute;n'></span> </a></li>
									</p>
								</div>";
								}
								//else
							?>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<div class="header">
			<a href="index.php"><img src="img/header.png" alt="LPTH"></a>
		</div>
	</header>
	<!--FinNavbar-->
	<!--MainContent-->
	<section class="main container">
		<div class="row">
			<section class="col-md-9 col-lg-9">
				<fieldset>
					<form class="form" action="activityManager.php" method="post" enctype="multipart/form-data">
						<h1>Gestor de Contenido <small>Crear Actividad</small></h1>
						<div class="form-group"><!--title-->
							<label for="title"><b>Título</b></label>
							<input class="form-control" type="text" name="title" id="title">
						</div>
							<div class="form-group">
								<label for="title"><b>Cabecera</b></label>
								<input type="file" class="form-control" name="imagen" id="imagen" size="30000" value=""/>
							</div>
						<div class="form-group"><!--content-->
							<label for="content"><b>Contenido</b></label>
							<textarea class="form-control" name="content" id="content" value="" rows="6" cols="100" maxlength="5000" placeholder=" Ingresa el contenido de la Noticia. Permite el ingreso de código HTML."></textarea>
						</div>
						<div class="form-group"><!--autor-->
							<label for="author"><b>Autor</b></label>
							<?php
							if(empty($_SESSION['username'])){
								header("Location: login.php");
								exit();
								/*SI TE ENVÍA A login.php ES PORQUE
								NO ESTÁS LOGUEADO, DUMBASS!*/
							}else{
								echo "<input type='text' class='form-control' name='author' id='author' required minlength='1' disabled value=".$_SESSION['username'].">";
							}
							?>
						</div>
						<div class="form-group"><!--date-->
							<label for="fecha"><b>Fecha</b></label>
							<?php
								date_default_timezone_get();
								$current = date("Y-m-d");
								echo "<input type='date' class='form-control' name='fecha' id='fecha' disabled value=".$current.">";
							?>
						</div>
						<div class="form-group"><!---->
							<label for="idCircuito"><b>Circuito</b></label>
							<select class="form-control" name='idCircuito' id='idCircuito' required>
								<option value=''>Selecciona un Circuito</option>
								<option value='1'>Zona Norte</option>
								<option value='2'>Zona Sur</option>
							</select>
						</div>
						<div class="form-group"><!--startdate-->
						<label for="startDate"><b>Fecha Inicio</b></label>
						<?php echo "<input type='date' class='form-group' name='startDate' id='startDate' min=".$current." required>"?>
						</div>
						<div class="form-group"><!--enddate-->
						<label for="endDate"><b>Fecha Fin</b></label>
						<?php echo "<input type='date' class='form-group' name='endDate' id='endDate' min=".$current." required>"?>
						</div>
						<!--enviar/reset-->
						<div class="form-group">
							<input class="btn btn-default btn-lg btn-block" type="submit" name="btnPublicar" value="Publicar Noticia">
						</div>
					</form>
				</fieldset>
			</section> 
			<!--Sidebar-->
			<aside class="col-md-3 col-lg-3 hidden-xs hidden-sm pull-right">
				<h4>Usuarios</h4>
				<div class="list-group">
					<?php
					if(empty($_SESSION['username'])){
						echo "<a class='list-group-item' href='login.php' class='list-group-item'>Iniciar Sesion</a>";
						echo "<a class='list-group-item' href='signup.php' class='list-group-item'>Crear Usuario</a>";
					}else{
						echo "<a href='perfil.php' class='list-group-item'>Mi Perfil</a>";
						echo "<a href='reportes.php' class='list-group-item'>Estadísticas</a>";
						echo "<a href='destroy.php' class='list-group-item'>Salir</a>";
					}
					?>
				</div>
				<h4>Actividades</h4>
				<div class="list-group">
					<a class="list-group-item" href="#">Más populares</a>
					<a class="list-group-item" href="entuzona.php">En tu zona</a>
					<a class="list-group-item" href="activityRetrieve.php">Más recientes</a>
					<br>
					<a class="list-group-item" href="zonanorte.php">Zona Norte</a>
					<a class="list-group-item" href="zonasur.php">Zona Sur</a>
				</div>
				<h4>Contacto</h4>
				<h4>Soporte Telefónico</h4>
				<p>(0261) 7778698<p/>
				<p>(0212) 7546939</p>
				<h4>Correo Electrónico</h4>
				<p>soporte@lapapelera.com</p>
			</aside>
			<!--FinSidebar-->
		</div>
	</section>
	<?php include "pieces/footer.php" ?>
	<script src="js/jquery-3.1.1.min.js"></script>
	<script src="js/bootstrap.js"></script>
	<script type="text/javascript" src='js/tinymce.js'></script>
</body>
</html>