<!DOCTYPE html>
<head>
	<meta charset="UTF-8">
	<title>La Papelera Tiene Hambre</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/index.css"/>
	<link rel="stylesheet" type="text/css" href="css/fonts.css">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="shortcut icon" type="image/png" href="img/ico.png"/>
</head>
<body>
	<!--Navbar-->
	<header>
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-1">
						<span class="sr-only">Menu</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a href="index.php" class="navbar-brand"><img src="img/brandicon.png" alt="LPTH" id="brandicon"></a>
				</div>
				<div class="collapse navbar-collapse" id="navbar-1"> 
					<ul class="nav navbar-nav lpth-navbar">
						<li class="active"><a href="index.php">INICIO</a></li>
						<li class=""><a href="retrieve_news.php">NOTICIAS</a></li>
						<li class=""><a href="activityRetrieve.php">ACTIVIDADES</a></li>
						<li class=""><a href="circuit_descrip.php">CIRCUITOS</a></li>
						<li class=""><a href="glossary.php">GLOSARIO</a></li>
						<li class=""><a href="contact.php">CONTACTO</a></li>
						<li><a>
							<?php
								session_start();
								if(empty($_SESSION['username'])){
									echo "<li><a href='login.php'>[Iniciar Sesión]</a></li>";
								}else{
								echo "
								<div class='btn-group hidden-sm hidden-xs pull-right'>
									</button>
									<ul class='dropdown-menu'>
									    	<li><a href='#'>Perfil</a></li>
								    		<li><a href='#'>Configuraci&oacute;n</a></li>
								    		<li role='separator' class='divider'></li>
								    		<li><a href='#'>Cerrar Sesi&oacute;n</a></li>
								  	</ul>
								</div>
								<div class='pull-right'>
									<li class='login.php'><a href='perfil.php'>[".$_SESSION['username']."]
										<span class='glyphicon glyphicon-user' data-toggle='tooltip' data-placement='bottom' title='Perfil y Configuraci&oacute;n'></span></a></li>
								</div>";
								}
								//else
							?>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<div class="header">
			<a href="index.php"><img src="img/header.png" alt="LPTH"></a>
		</div>
	</header>
	<!--FinNavbar-->
	<!--MainContent-->
	<section class="main container">
		<div class="row">
			<section class="col-md-9 col-lg-9">
				<?php
					$servername = 'localhost';
					$username = 'root';
					$password = '';
					// Create connection
					$conn = mysqli_connect($servername,$username,$password,'lapapelera');
					// Check connection
					if($conn === false){
					    die('ERROR: Could not connect. '.mysqli_connect_error());
					}else{
						echo '<h2><center>ESTADÍSTICAS DEL SITIO</center></h2><hr style="border-top: dotted 1px;width:30%;color:#c0c0c0">';	
					}

					mysqli_select_db($conn,'lapapelera');
					mysqli_set_charset($conn,"utf8");

					echo "<div class='list-group'>";
					echo "<span class='list-group'><b>NOTICIAS</b></span>";//noti  
					//total
					$result = mysqli_query($conn," SELECT COUNT(*) as total FROM noticia;");
					$row = mysqli_fetch_assoc($result);
						echo "<span class='list-group-item'>Número de Noticias Registradas: ",$row['total'],"</span>";
					//last
					$query = "SELECT * FROM noticia ORDER BY idNoticia DESC LIMIT 1";
					$result = mysqli_query($conn,$query);
					while($row = mysqli_fetch_array($result)){
						$idNoticia = $row['idNoticia'];
						$titulo = $row['titulo'];
						echo "<span class='list-group-item'>Última Noticia Registrada: <a href='getnew.php?id=".$idNoticia."''>$titulo</a></span>";
					}//if
					//authors
					$query = "SELECT COUNT(DISTINCT autor) AS total FROM noticia";
					$result = mysqli_query($conn,$query);
					$values = mysqli_fetch_assoc($result); 
					$numAutores = $values['total'];
						echo "<span class='list-group-item'>Las Noticias han sido publicadas por un total de <b>",$numAutores,"</b> usuarios.</span>";

					echo "<br><span class='list-group'><b>ACTIVIDADES</b></span>";//act
					$result = mysqli_query($conn," SELECT COUNT(*) as total FROM actividad");
					$row = mysqli_fetch_assoc($result);  
						echo "<span class='list-group-item'>Número de Actividades Registradas: ",$row['total'],"</span>";
					$query = "SELECT * FROM actividad ORDER BY idActividad DESC LIMIT 1";
					$result = mysqli_query($conn,$query);
					while($row = mysqli_fetch_array($result)){
						$idActividad = $row['idActividad'];
						$titulo = $row['titulo'];
						echo "<span class='list-group-item'>Última Actividad Registrada: <a href='getact.php?id=".$idActividad."''>$titulo</a></span>";
					}//if
					$result = mysqli_query($conn," SELECT COUNT(*) as total FROM actividad WHERE idCircuito=1;");
					$row = mysqli_fetch_assoc($result);  
						echo "<span class='list-group-item'>Actividades Realizadas en el Circuito Norte: ",$row['total'],"</span>";
					$result = mysqli_query($conn," SELECT COUNT(*) as total FROM actividad WHERE idCircuito=2;");
					$row = mysqli_fetch_assoc($result);  
						echo "<span class='list-group-item'>Actividades Realizadas en el Circuito Sur: ",$row['total'],"</span>";
					//authors
					$query = "SELECT COUNT(DISTINCT autor) AS total FROM actividad";
					$result = mysqli_query($conn,$query);
					$values = mysqli_fetch_assoc($result); 
					$numAutores = $values['total'];
						echo "<span class='list-group-item'>Las Actividades han sido publicadas por un total de <b>",$numAutores,"</b> usuarios.</span>";

					echo "<br><span class='list-group'><b>USUARIOS</b></span>";//usuarios
					$result = mysqli_query($conn," SELECT COUNT(*) as total FROM usuario;");
					$row = mysqli_fetch_assoc($result);  
						echo "<span class='list-group-item'>Número de Usuarios Registrados: ",$row['total'],"</span>";
						echo "<span class='list-group-item'>Último Usuario Registrado: ",$row['total'],"</span>";
					$result = mysqli_query($conn," SELECT COUNT(*) as total FROM usuario WHERE idCircuito=1;");
					$row = mysqli_fetch_assoc($result);  
						echo "<span class='list-group-item'>Usuarios adscritos al Circuito Norte: ",$row['total'],"</span>";
					$result = mysqli_query($conn," SELECT COUNT(*) as total FROM usuario WHERE idCircuito=2;");
					$row = mysqli_fetch_assoc($result);  
						echo "<span class='list-group-item'>Usuarios adscritos al Circuito Sur: ",$row['total'],"</span>";


					echo "<br><span class='list-group'><b>COMENTARIOS</b></span>";//COMENTARIOS
					//total
					$query = "SELECT COUNT(*) AS total FROM comentario";
					$result = mysqli_query($conn,$query);
					$values = mysqli_fetch_assoc($result); 
					$numComments = $values['total'];
						echo "<span class='list-group-item'>Número de Comentarios Realizados: ",$numComments,"</span>";
					$query = "SELECT COUNT(DISTINCT autor) AS total FROM comentario";
					$result = mysqli_query($conn,$query);
					$values = mysqli_fetch_assoc($result); 
					$numComments = $values['total'];
						echo "<span class='list-group-item'>Los comentarios han sido realizados por un total de <b>",$numComments,"</b> usuarios.</span>";


					echo "</div>";
				?>
			</section> 
			<!--Sidebar-->
			<aside class="col-md-3 col-lg-3 hidden-xs hidden-sm pull-right">
				<h4>Usuarios</h4>
				<div class="list-group">
					<?php
					if(empty($_SESSION['username'])){
						 echo "<a class='list-group-item' href='login.php' class='list-group-item'>Iniciar Sesion</a>";
						 echo "<a class='list-group-item' href='signup.php' class='list-group-item'>Crear Usuario</a>";
					}else{
						 echo "<a href='perfil.php' class='list-group-item'>Mi Perfil</a>";
						 echo "<a href='reportes.php' class='list-group-item'>Reportes</a>";
						 echo "<a href='destroy.php' class='list-group-item'>Salir</a>";
					}
					?>
				</div>
				<h4>Actividades</h4>
				<div class="list-group">
					<a class="list-group-item" href="#">Más populares</a>
					<a class="list-group-item" href="entuzona.php">En tu zona</a>
					<a class="list-group-item" href="activityRetrieve.php">Más recientes</a>
					<br>
					<a class="list-group-item" href="zonanorte.php">Zona Norte</a>
					<a class="list-group-item" href="zonasur.php">Zona Sur</a>
				</div>
				<h4>Contacto</h4>
				<h4>Soporte Telefónico</h4>
				<p>(0261) 7778698<p/>
				<p>(0212) 7546939</p>
				<h4>Correo Electrónico</h4>
				<p>soporte@lapapelera.com</p>
			</aside>
			<!--FinSidebar-->
		</div>
	</section>
	<?php include "pieces/footer.php" ?>
	<script src="js/jquery-3.1.1.min.js"></script>
	<script src="js/bootstrap.js"></script>
</body>
</html>