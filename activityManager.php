<html>

	<head>
		<meta charset="UTF-8">
		<title>Zona PHP</title>
	</head>
	<body>

	<?php 
		$servername = "localhost";
		$username = "root";
		$password = "";
		// Create connection
		$conn = mysqli_connect($servername,$username,$password,"lapapelera");
		// Check connection
		if($conn === false){
		    die("ERROR: Could not connect. ".mysqli_connect_error());
		}else{
			echo "-- [Conexión exitosa]","<br>";
		}

		mysqli_select_db($conn,"lapapelera");
		session_start();

		$title 		= $_POST['title'];
		$descrip 	= $_POST['descripcion'];
		$idActividad= $_POST['idActividad'];
		$serv_default_date = date_default_timezone_get();
		$fecha 		= date('Y-m-d');
		$autor 		= $_SESSION['username'];
		$startDate 	= $_POST['startDate'];
		$endDate 	= $_POST['endDate'];
		$idCircuito	= $_POST['idCircuito'];
		$imagenName = $_FILES['imagen']['name'];
		if($imagenName===""){
			$ruta 	= "multimedia/default.png";
		}else{
			$ruta 	= "multimedia/".$_FILES['imagen']['name'];
		}

		//comprobamos si ha ocurrido un error.
		if ($_FILES["imagen"]["error"] > 0){
			echo "-- [HA OCURRIDO UN ERROR PHP] -- Código: ";
			echo $_FILES["imagen"]["error"];
		}//if
		else{
			//ahora vamos a verificar si el tipo de archivo es un tipo de imagen permitido.
			//y que el tamano del archivo no exceda los 100kb
			$permitidos = array("image/jpg", "image/jpeg", "image/gif", "image/png");
			$limite_kb = 30000;
			if(in_array($_FILES['imagen']['type'],$permitidos) && $_FILES['imagen']['size']<=$limite_kb*1024){
				//esta es la ruta donde copiaremos la imagen
				//recuerden que deben crear un directorio con este mismo nombre
				//en el mismo lugar donde se encuentra el archivo subir.phpp
				$ruta = "multimedia/".$_FILES['imagen']['name'];
				//comprobamos si este archivo existe para no volverlo a copiar.
				//pero si quieren pueden obviar esto si no es necesario.
				//o pueden darle otro nombre para que no sobreescriba el actual.
				if (!file_exists($ruta)){
					//aqui movemos el archivo desde la ruta temporal a nuestra ruta usamos la variable $resultado para almacenar el resultado del proceso de mover el archivo almacenara true o false
					$resultado = move_uploaded_file($_FILES["imagen"]["tmp_name"], $ruta);
					if($resultado){
						echo move_uploaded_file($_FILES["imagen"]["tmp_name"], $ruta);
						echo "el archivo ha sido movido exitosamente";
					}else{
						echo "ocurrio un error al mover el archivo.";
						header("Location: activity.php");
						exit();

					}//else
				}else{
					echo "--ERROR: El archivo ".$_FILES['imagen']['name']." ya existe";
				}//else
			}else{
				echo "archivo no permitido, es tipo de archivo prohibido o excede el tamano de $limite_kb Kilobytes";
				header("Location: activity.php");
				exit();
			}//else
		}//else

		echo "<br>";
		echo "<br>";
		echo "TÍTULO: $title<br>";
		echo "DESCRIPCIÓN: $descrip<br>";
		echo "FECHA: $fecha<br>";
		echo "FECHA INICIO: $startDate<br>";
		echo "FECHA FIN: $endDate<br>";
		echo "ID Circuito: $idCircuito<br>";
		echo "RUTA IMAGEN: $ruta<br>";
		echo "AUTOR: $autor<br>";
		echo "<br>";

		if($startDate>$endDate){
			header("Location: activity.php");
		}else{
			if(empty($idActividad))
				$query = "INSERT INTO actividad(titulo,descripcion,fecha,startDate,endDate,imagen,autor,idCircuito) VALUES('$title','$descrip','$fecha','$startDate','$endDate','$ruta','$autor',$idCircuito)";
			else
				$query = "UPDATE actividad SET titulo='$title',imagen='$ruta',contenido='$content',startDate='$startDate',endDate='$endDate' WHERE idActividad='$idActividad'";
			mysqli_query($conn,$query) or die ('-- [HA OCURRIDO UN ERROR MYSQL]: '.mysqli_error($conn));

			//if you show the query, it won't redirect...
			// echo "<hr><br><b>Query</b>: $query<br>";
			echo "<br>","[ACTIVIDAD REGISTRADA CON ÉXITO]","<br>";
			header("Location: activityRetrieve.php");
			exit();
		}//else
	?>
</body>
</html>