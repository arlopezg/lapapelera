<!DOCTYPE html>
<head>
	<meta charset="UTF-8">
	<title>La Papelera Tiene Hambre</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/index.css"/>
	<link rel="stylesheet" type="text/css" href="css/contact.css"/>
	<link rel="stylesheet" type="text/css" href="css/fonts.css">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="shortcut icon" type="image/png" href="img/ico.png"/>
</head>
<body>
	<header>
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-1">
						<span class="sr-only">Menu</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a href="index.php" class="navbar-brand"><img src="img/brandicon.png" alt="LPTH" id="brandicon"></a>
				</div>
				<div class="collapse navbar-collapse" id="navbar-1"> 
					<ul class="nav navbar-nav lpth-navbar">
						<li class=""><a href="index.php">INICIO</a></li>
						<li class=""><a href="retrieve_news.php">NOTICIAS</a></li>
						<li class=""><a href="activityRetrieve.php">ACTIVIDADES</a></li>
						<li class=""><a href="circuit_descrip.php">CIRCUITOS</a></li>
						<li class=""><a href="glossary.php">GLOSARIO</a></li>
						<li class="active"><a href="contact.php">CONTACTO</a></li>
						<li><a>
							<?php
								session_start();
								if(empty($_SESSION['username'])){
									echo "<li><a href='login.php'>[Iniciar Sesión]</a></li>";
								}else{
								echo "
								<div class='btn-group hidden-sm hidden-xs pull-right'>
									</button>
									<ul class='dropdown-menu'>
									    	<li><a href='#'>Perfil</a></li>
								    		<li><a href='#'>Configuraci&oacute;n</a></li>
								    		<li role='separator' class='divider'></li>
								    		<li><a href='#'>Cerrar Sesi&oacute;n</a></li>
								  	</ul>
								</div>
								<div class='pull-right'>
									<li class='login.php'><a href='perfil.php'>[".$_SESSION['username']."]
										<span class='glyphicon glyphicon-user' data-toggle='tooltip' data-placement='bottom' title='Perfil y Configuraci&oacute;n'></span> </a></li>
									</p>
								</div>";
								}
								//else
							?>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<div class="header">
			<a href="index.php"><img src="img/header.png" alt="LPTH"></a>
		</div>
	</header>
	<!--form-->	
	<div class="jumbotron jumbotron-fluid">
		<div class="container">
			<h1><span style="font-family: Montserrat; font-size: 48px;">Reduce. Reutiliza. Recicla.</span></h1>
			<p class="lead">
				La ciudad más limpia no es la que más se barre, sino la que menos <strong>ensucias</strong>.
				<br>
				Fundaci&oacute;n #LaPapeleraTieneHambre
			</p>
		</div>
	</div>
	<section class="main container">
		<div class="row">
			<div class="col-md-8 col-lg-9 clearfix">
				<div class="col-sm-6 col-md-4">
					<h4><span class="fa fa-users" style="font-size: 24px;"></span>&nbsp; Misi&oacute;n</h4>
					<p>Concienciar las personas que botan la basura al suelo, por la ventana del carro, al lago, las playas, parques, plazas o en cualquier espacio público.</p>
				</div>
				<div class="col-sm-6 col-md-4">
					<h4><span class="fa fa-eye" style="font-size: 24px;"></span>&nbsp; Visi&oacute;n</h4>
					<p>Fundado el 20 de agosto de 2012. Somos movimiento que busca mostrar al ciudadano la importancia de llevar un equilibrio con el medio ambiente.</p>
				</div>
				<div class="col-sm-6 col-md-4">
					<h4><span class="fa fa-heart" style="font-size: 24px;"></span>&nbsp; Valores</h4>
					<p>Responsbilidad. Solidaridad. Compromiso. Iniciativa. Empat&iacute;a. Queremos, corregimos, necesitamos una ciudad limpia. </p>
				</div>
				<div class="col-sm-6 col-md-4">
					<h4><span class="fa fa-address-book" style="font-size: 24px;"></span>&nbsp; N&uacute;meros de Contacto</h4>
					<p>(0261) 6663312</p>
				</div>
				<div class="col-sm-6 col-md-4">
					<h4><span class="fa fa-envelope" style="font-size: 24px;"></span>&nbsp; Correos Electr&oacute;nicos</h4>
					<p><a href="mailto:lapapeleratienehambre@gmail.com">lapapeleratienehambre@gmail.com</a></p>
				</div>
				<div class="col-sm-6 col-md-4">
					<h4><span class="fa fa-map" style="font-size: 24px;"></span>&nbsp; Direcci&oacute;n</h4>
					<p>Urbanizacion "Piloto" Ciudadela Far&iacute;a</p>
				</div>
			</div>
			<div class="row">
				<h3>Urbanizacion Piloto</h3>
				<p>Ciudadela Far&iacute;a</p>
			</div>
		</div>

	</section>
	<?php include "pieces/footer.php" ?>
	<script src="js/jquery-3.1.1.min.js"></script>
	<script src="js/bootstrap.js"></script>
</body>
</html>