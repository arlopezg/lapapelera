<!DOCTYPE html>
<head>
	<meta charset="UTF-8">
	<title>La Papelera Tiene Hambre</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/index.css"/>
	<link rel="stylesheet" type="text/css" href="css/fonts.css">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="shortcut icon" type="image/png" href="img/ico.png"/>
</head>

<body>
	<header>
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-1">
						<span class="sr-only">Menu</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a href="index.php" class="navbar-brand"><img src="img/brandicon.png" alt="LPTH" id="brandicon"></a>
				</div>
				<div class="collapse navbar-collapse" id="navbar-1"> 
					<ul class="nav navbar-nav">
						<li class=""><a href="index.php">INICIO</a></li>
						<li class=""><a href="retrieve_news.php">NOTICIAS</a></li>
						<li class=""><a href="activityRetrieve.php">ACTIVIDADES</a></li>
						<li class=""><a href="circuit_descrip.php">CIRCUITOS</a></li>
						<li class=""><a href="glossary.php">GLOSARIO</a></li>
						<li class=""><a href="contact.php">CONTACTO</a></li>
						<li><a>
							<?php
								session_start();
								if(empty($_SESSION['username'])){
									echo "<li><a href='login.php'>[Iniciar Sesión]</a></li>";
								}else{
								echo "
								<div class='btn-group hidden-sm hidden-xs pull-right'>
									</button>
									<ul class='dropdown-menu'>
									    	<li><a href='#'>Perfil</a></li>
								    		<li><a href='#'>Configuraci&oacute;n</a></li>
								    		<li role='separator' class='divider'></li>
								    		<li><a href='#'>Cerrar Sesi&oacute;n</a></li>
								  	</ul>
								</div>
								<div class='pull-right'>
									<li class='login.php'><a href='perfil.php'>[".$_SESSION['username']."]
										<span class='glyphicon glyphicon-user' data-toggle='tooltip' data-placement='bottom' title='Perfil y Configuraci&oacute;n'></span> </a></li>
									</p>
								</div>";
								}
								//else
							?>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<div class="header">
			<a href="index.php"><img src="img/header.png" alt="LPTH"></a>
		</div>
	</header>
	<!--form-->	
	<div class="main container">
		<section class="container-fluid row">
			<div class="form-signin col-md-9 col-lg-9 clearfix">
				<form id="formReg" action="session.php" method="post">
					<h2 class="form-signin-heading"><span class="lpth">Iniciar Sesion</span></h2>
					<label for="usernameusername" class="sr-only">Nombre de Usuario</label>
					<input type="text" id="username" name="username" class="form-control" placeholder="Nombre de Usuario" required autofocus maxlength="30" value="" autofocus>
					<br>
					<label for="pword" class="sr-only">Contraseña</label>
					<input type="password" id="pword" name="pword" class="form-control" placeholder=" Contrase&nacute;a" required maxlength="30" value="">
					<br>
					<button class="btn btn-lg btn-success btn-block" id="btnLogin" name="btnLogin" type="submit" value="Iniciar sesi&oacute;n">Iniciar Sesión</button>
				</form>
				<center>
					<h2 class="form-signin-heading"><span class="lpth">¿Nuevo por aqu&iacute;?</span></h2>
				</center>
				<button class="btn btn-lg btn-warning btn-block">
					<a href="signup.php" style="text-decorations:none">Regístrate</a>
				</button>
			</div>
			<aside class="col-md-3 col-lg-3 hidden-xs hidden-sm pull-right">
				<h4>Registro</h4>
				<div class="list-group">
					<a class="list-group-item active lpth" href="login.php">Iniciar Sesion</a>
					<a class="list-group-item" href="signup.php">Crear Usuario</a>
				</div>

				<h4>Actividades</h4>
				<div class="list-group">
					<a class="list-group-item" href="#">Más populares</a>
					<a class="list-group-item" href="#">En tu zona</a>
					<a class="list-group-item" href="activityRetrieve.php">Más recientes</a>
					<br>
					<a class="list-group-item" href="zonanorte.php">Zona Norte</a>
					<a class="list-group-item" href="zonasur.php">Zona Sur</a>
				</div>
				<br>
				<h4>Contacto</h4>
				<h4>Soporte Telefónico</h4>
				<p>(0261) 7778698<p/>
				<p>(0212) 7546939</p>
				<h4>Correo Electrónico</h4>
				<p>soporte@lapapelera.com</p>
			</aside>
		</section>
	</div>
	<footer>
		<div class="container foot">
			<p id="footext">
				La Papelera Tiene Hambre.
				<br>
				Ingeniería del Software II. 2016.
			</p>
		</div>
	</footer>
	<script src="js/jquery-3.1.1.min.js"></script>
	<script src="js/bootstrap.js"></script>
</body>
</html>