<!DOCTYPE html>
<head>
	<meta charset="UTF-8">
	<title>La Papelera Tiene Hambre</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/index.css"/>
	<link rel="stylesheet" type="text/css" href="css/fonts.css">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="shortcut icon" type="image/png" href="img/ico.png"/>
</head>
<body>
	<!--Navbar-->
	<header>
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-1">
						<span class="sr-only">Menu</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a href="index.php" class="navbar-brand"><img src="img/brandicon.png" alt="LPTH" id="brandicon"></a>
				</div>
				<div class="collapse navbar-collapse" id="navbar-1"> 
					<ul class="nav navbar-nav lpth-navbar">
						<li class=""><a href="index.php">INICIO</a></li>
						<li class=""><a href="retrieve_news.php">NOTICIAS</a></li>
						<li class=""><a href="activityRetrieve.php">ACTIVIDADES</a></li>
						<li class="active"><a href="circuit_descrip.php">CIRCUITOS</a></li>
						<li class=""><a href="glossary.php">GLOSARIO</a></li>
						<li class=""><a href="contact.php">CONTACTO</a></li>
						<li><a>
							<?php
								session_start();
								if(empty($_SESSION['username'])){
									echo "<li><a href='login.php'>[Iniciar Sesión]</a></li>";
								}else{
								echo "
								<div class='btn-group hidden-sm hidden-xs pull-right'>
									</button>
									<ul class='dropdown-menu'>
									    	<li><a href='#'>Perfil</a></li>
								    		<li><a href='#'>Configuraci&oacute;n</a></li>
								    		<li role='separator' class='divider'></li>
								    		<li><a href='#'>Cerrar Sesi&oacute;n</a></li>
								  	</ul>
								</div>
								<div class='pull-right'>
									<li class='login.php'><a href='perfil.php'>[".$_SESSION['username']."]
										<span class='glyphicon glyphicon-user' data-toggle='tooltip' data-placement='bottom' title='Perfil y Configuraci&oacute;n'></span> </a></li>
									</p>
								</div>";
								}
								//else
							?>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<div class="header">
			<a href="index.php"><img src="img/header.png" alt="LPTH"></a>
		</div>
	</header>
	<!--FinNavbar-->
	<!--MainContent-->
	<section class="main container">
		<div class="row">
			<section class="col-md-9 col-lg-9">
				<article>
					<div class="panel panel-default">
					<h2 class="panel-heading">¿Qué son los Circuitos?</h2>
					<p class="panel-body">Los Circuitos son lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto, magni, aspernatur! Vel, voluptates autem similique officia tempora repellat. Fugiat, atque dicta voluptatibus ipsam dignissimos vero expedita cum dolores eum eligendi.</p>
					</div>
					<div class="panel panel-default">
					<h2 class="panel-heading">¿Cómo funciona un Circuito?</h2>
					<p class="panel-body">Nuestros Circuitos se manejan de la siguiente manera: lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, deserunt facere voluptates fuga eveniet quisquam fugiat, repellendus, corporis excepturi molestiae repellat, neque aspernatur maxime illum culpa natus impedit dicta iure!</p>
					</div>
					<div class="panel panel-default">
					<h2 class="panel-heading">¿Puedo unirme a cualquier Circuito disponible?</h2>
					<p class="panel-body">Cuentas con la opción de agregarte a cualquiera de los dos Circuitos (<a href="zonanorte.php">Norte</a> y <a href="zonasur.php">Sur</a>) pero Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident ipsum aut ipsa explicabo animi voluptas beatae sit possimus quibusdam iure magni, facilis obcaecati assumenda, nam laborum dignissimos iusto praesentium doloribus.</p>
					</div>
				</article>
			</section> 
			<!--Sidebar-->
			<aside class="col-md-3 col-lg-3 hidden-xs hidden-sm pull-right">
				<h4>Usuarios</h4>
				<div class="list-group">
					<?php
					if(empty($_SESSION['username'])){
						echo "<a class='list-group-item' href='login.php' class='list-group-item'>Iniciar Sesion</a>";
						echo "<a class='list-group-item' href='signup.php' class='list-group-item'>Crear Usuario</a>";
					}else{
						echo "<a href='perfil.php' class='list-group-item'>Mi Perfil</a>";
						echo "<a href='reportes.php' class='list-group-item'>Estadísticas</a>";
						echo "<a href='destroy.php' class='list-group-item'>Salir</a>";
					}
					?>
				</div>
				<h4>Actividades</h4>
				<div class="list-group">
					<a class="list-group-item" href="#">Más populares</a>
					<a class="list-group-item" href="entuzona.php">En tu zona</a>
					<a class="list-group-item" href="activityRetrieve.php">Más recientes</a>
					<br>
					<a class="list-group-item" href="zonanorte.php">Zona Norte</a>
					<a class="list-group-item" href="zonasur.php">Zona Sur</a>
				</div>
				<h4>Contacto</h4>
				<h4>Soporte Telefónico</h4>
				<p>(0261) 7778698<p/>
				<p>(0212) 7546939</p>
				<h4>Correo Electrónico</h4>
				<p>soporte@lapapelera.com</p>
			</aside>
			<!--FinSidebar-->
		</div>
	</section>
	<?php include "pieces/footer.php" ?>
	<script src="js/jquery-3.1.1.min.js"></script>
	<script src="js/bootstrap.js"></script>
</body>
</html>