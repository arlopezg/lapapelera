<html>
	<head>
		<meta charset="UTF-8">
		<title>Zona PHP</title>
	</head>
	<body>

	<?php 
		$servername = "localhost";
		$username = "root";
		$password = "";
		// Create connection
		$conn = mysqli_connect($servername,$username,$password,"lapapelera");
		// Check connection
		if($conn === false){
		    die("ERROR: Could not connect. ".mysqli_connect_error());
		}else{
			echo "-- [Conexión exitosa]","<br>";
		}

		mysqli_select_db($conn,"lapapelera");
		session_start();

		$title 		= $_POST['title'];
		$content 	= $_POST['content'];
		$idNoticia 	= $_POST['idNoticia'];
		$serv_default_date = date_default_timezone_get();
		$fecha 		= date('Y-m-d');
		$autor 		= $_SESSION['username'];
		$imagenName = $_FILES['imagen']['name'];
		if($imagenName===""){
			$ruta 	= "multimedia/default.png";
		}else{
			$ruta 	= "multimedia/".$_FILES['imagen']['name'];
		}

		echo "<br>";
		echo "ID noticia: $idNoticia<br>";
		echo "TÍTULO: $title<br>";
		echo "FECHA: $fecha<br>";
		echo "IMAGEN URL: $ruta<br>";
		echo "AUTOR: $autor<br>";

		//comprobamos si ha ocurrido un error.
		if ($_FILES["imagen"]["error"] > 0){
			echo "<hr>Ha ocurrido un error al subir el archivo. PHP Upload Error #";
			echo $_FILES["imagen"]["error"];
		}
		else{
			//ahora vamos a verificar si el tipo de archivo es un tipo de imagen permitido.
			//y que el tamano del archivo no exceda los 100kb
			$permitidos = array("image/jpg", "image/jpeg", "image/gif", "image/png");
			$limite_kb = 30000;

			if (in_array($_FILES['imagen']['type'],$permitidos) && $_FILES['imagen']['size']<=$limite_kb*1024){
				//esta es la ruta donde copiaremos la imagen
				//recuerden que deben crear un directorio con este mismo nombre
				//en el mismo lugar donde se encuentra el archivo subir.php
				$ruta = "multimedia/".$_FILES['imagen']['name'];
				//comprovamos si este archivo existe para no volverlo a copiar.
				//pero si quieren pueden obviar esto si no es necesario.
				//o pueden darle otro nombre para que no sobreescriba el actual.
				if (!file_exists($ruta)){
					//aqui movemos el archivo desde la ruta temporal a nuestra ruta usamos la variable $resultado para almacenar el resultado del proceso de mover el archivo almacenara true o false
					$resultado = move_uploaded_file($_FILES["imagen"]["tmp_name"], $ruta);
					if($resultado){
						echo move_uploaded_file($_FILES["imagen"]["tmp_name"], $ruta);
						echo "El archivo ha sido movido exitosamente";
					} else {
						echo "Ocurrió un error al mover el archivo.";
						header("Location: news.php");
						exit();
					}
				} else {
					echo "--ERROR: El archivo ".$_FILES['imagen']['name']." ya existe";
				}
			} else {
				echo "Archivo no permitido. Es tipo de archivo prohibido o excede el tamano de $limite_kb Kilobytes";
				header("Location: news.php");
				exit();
			}
		}


		if(empty($idNoticia))
			$query = "INSERT INTO noticia(titulo,imagen,contenido,fecha,autor) VALUES('$title','$ruta','$content','$fecha','$autor')";
		else
			$query = "UPDATE noticia SET titulo='$title',imagen='$ruta',contenido='$content' WHERE idNoticia='$idNoticia'";
		mysqli_query($conn,$query) or die ('-- [Ha ocurrido un error]: '.mysqli_error($conn));
		//if you show the query, it won't redirect...
		// echo "<hr><br><b>Query</b>: $query<br>";
		echo "<hr><b>[NOTICIA REGISTRADA CON ÉXITO]</b>";
		echo "<br><a href='retrieve_news.php'>Regresar</a>";
		header("Location: retrieve_news.php");
		exit();
	?>

</body>
</html>