<!DOCTYPE html>
<head>
	<meta charset="UTF-8">
	<title>La Papelera Tiene Hambre</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/index.css"/>
	<link rel="stylesheet" type="text/css" href="css/fonts.css">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="shortcut icon" type="image/png" href="img/ico.png"/>
</head>
<body>
	<!--Navbar-->
	<header>
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-1">
						<span class="sr-only">Menu</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a href="index.php" class="navbar-brand"><img src="img/brandicon.png" alt="LPTH" id="brandicon"></a>
				</div>
				<div class="collapse navbar-collapse" id="navbar-1"> 
					<ul class="nav navbar-nav lpth-navbar">
						<li class=""><a href="index.php">INICIO</a></li>
						<li class=""><a href="retrieve_news.php">NOTICIAS</a></li>
						<li class="active"><a href="activityRetrieve.php">ACTIVIDADES</a></li>
						<li class=""><a href="circuit_descrip.php">CIRCUITOS</a></li>
						<li class=""><a href="glossary.php">GLOSARIO</a></li>
						<li class=""><a href="contact.php">CONTACTO</a></li>
						<li><a>
							<?php
								session_start();
								if(empty($_SESSION['username'])){
									echo "<li><a href='login.php'>[Iniciar Sesión]</a></li>";
								}else{
								echo "
								<div class='btn-group hidden-sm hidden-xs pull-right'>
									</button>
									<ul class='dropdown-menu'>
									    	<li><a href='#'>Perfil</a></li>
								    		<li><a href='#'>Configuraci&oacute;n</a></li>
								    		<li role='separator' class='divider'></li>
								    		<li><a href='#'>Cerrar Sesi&oacute;n</a></li>
								  	</ul>
								</div>
								<div class='pull-right'>
									<li class='login.php'><a href='perfil.php'>[".$_SESSION['username']."]
										<span class='glyphicon glyphicon-user' data-toggle='tooltip' data-placement='bottom' title='Perfil y Configuraci&oacute;n'></span> </a></li>
									</p>
								</div>";
								}
								//else
							?>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<div class="header">
			<a href="index.php"><img src="img/header.png" alt="LPTH"></a>
		</div>
	</header>
	<!--FinNavbar-->
	<!--MainContent-->
	<section class="main container">
		<div class="row">
			<section class="col-md-9 col-lg-9">
				<article class="post page-header clearfix">
					<?php
					//listar acts
					$conn = mysqli_connect('localhost','root','','lapapelera');
					$query  = "SELECT * FROM usuario WHERE username = '".$_SESSION['username']."'";
					$result = mysqli_query($conn,$query);
					mysqli_select_db($conn,'lapapelera');
					mysqli_set_charset($conn,"utf8");
					if(!empty($_SESSION['username'])){//si el usuario NO está desconectado...
						$userTypeQuery  = "SELECT * FROM usuario WHERE username = '".$_SESSION['username']."'";
						$userType = mysqli_query($conn,$userTypeQuery);
						while($row = mysqli_fetch_array($userType)){
								if($row['idCircuito']==1) 	$idCircuito = 1;
								else 						$idCircuito = 2;
							}//while
						}//if
					
					//listar noticias
					$query = "SELECT * FROM actividad WHERE idCircuito = ".$idCircuito.";";
					echo "<h2>Estas son la Actividades cercanas a ti</h3>";
					$result = mysqli_query($conn,$query);

					while($row = mysqli_fetch_array($result)){
						//count comments
						$busqueda = "SELECT COUNT(*) AS total FROM comentario where idActividad = ".$row['idActividad'];
						$resultado = mysqli_query($conn,$busqueda);
						$values = mysqli_fetch_assoc($resultado); 
						$numComments = $values['total'];
						// strip tags to avoid breaking any html
						$strippedContent = $row['descripcion'];
						$strippedContent = strip_tags($strippedContent);
						//Cortar contenido a 300 chars, mostrar "Leer Más"
						if (strlen($strippedContent)>110){
						    // truncate string
						    $stringCut = substr($strippedContent,0,110);
						    // make sure it ends in a word so assassinate doesn't become ass...
						    $strippedContent = substr($stringCut,0,strrpos($stringCut,' '))."... <a href='getact.php?id=".$row['idActividad']."'>Leer Más.</a>"; 
						}//if
						//Mostrar c/Noticia
						echo "<article class='post page-header clearfix'>";
						/*Imagen*/
						echo "<a href='getact.php?id=".$row['idActividad']."' class='thumb pull-left'><img class='img-thumbnail' src='".$row['imagen']."'' alt='Imagen de Actividad'></a>";
						/*Titulo*/
						echo "<h2 class='post-title'><a href='getact.php?id=".$row['idActividad']."'>".$row['titulo']."</a></h2>";
						/*Publicado [fecha] por [autor]*/
						echo "<p>Publicado el ".$row['fecha']."</span> por <span><a href='$'>".$row['autor']."</a></p>";
						echo "<p>Fecha Inicio: ".$row['startDate']."</p>";
						echo "<p>Fecha Fin: ".$row['endDate']."</p>";
						echo "<p>Realizado en la ";//...
						if($row['idCircuito']==1)	echo "Zona Norte</p>";
						else 						echo "Zona Sur</p>";
						echo"<br>";
						/*Contenido*/
						echo "<p class='post-content text-justify'>".$strippedContent."</p>";
						echo "<div class='container-buttons'>";
						echo "<a href='getact.php?id=".$row['idActividad']."' class='btn btn-md btn-primary'>Leer Mas</a>";
						echo "<a href='getact.php?id=".$row['idActividad']."' class='btn btn-md btn-success'>Comentarios <span class='badge'>".$numComments."</span></a>";
						echo "</div>";
						echo "</article>";
					}//if
					?>
				</article>
			</section> 
			<!--Sidebar-->
			<aside class="col-md-3 col-lg-3 hidden-xs hidden-sm pull-right">
				<h4>Usuarios</h4>
				<div class="list-group">
					<?php
					if(empty($_SESSION['username'])){
						echo "<a class='list-group-item' href='login.php' class='list-group-item'>Iniciar Sesion</a>";
						echo "<a class='list-group-item' href='signup.php' class='list-group-item'>Crear Usuario</a>";
					}else{
						echo "<a href='perfil.php' class='list-group-item'>Mi Perfil</a>";
						echo "<a href='reportes.php' class='list-group-item'>Estadísticas</a>";
						echo "<a href='destroy.php' class='list-group-item'>Salir</a>";
					}
					?>
				</div>
				<h4>Actividades</h4>
				<div class="list-group">
					<a class="list-group-item" href="#">Más populares</a>
					<a class="list-group-item" href="entuzona.php">En tu zona</a>
					<a class="list-group-item" href="activityRetrieve.php">Más recientes</a>
					<br>
					<a class="list-group-item" href="zonanorte.php">Zona Norte</a>
					<a class="list-group-item" href="zonasur.php">Zona Sur</a>
				</div>
				<h4>Contacto</h4>
				<h4>Soporte Telefónico</h4>
				<p>(0261) 7778698<p/>
				<p>(0212) 7546939</p>
				<h4>Correo Electrónico</h4>
				<p>soporte@lapapelera.com</p>
			</aside>
			<!--FinSidebar-->
		</div>
	</section>
	<?php include "pieces/footer.php" ?>
	<script src="js/jquery-3.1.1.min.js"></script>
	<script src="js/bootstrap.js"></script>
</body>
</html>