<!DOCTYPE html>
<head>
	<meta charset="UTF-8">
	<title>La Papelera Tiene Hambre</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/index.css"/>
	<link rel="stylesheet" type="text/css" href="css/fonts.css">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="shortcut icon" type="image/png" href="img/ico.png"/>
</head>
<body>
	<!--Navbar-->
	<header>
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-1">
						<span class="sr-only">Menu</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a href="index.php" class="navbar-brand"><img src="img/brandicon.png" alt="LPTH" id="brandicon"></a>
				</div>
				<div class="collapse navbar-collapse" id="navbar-1"> 
					<ul class="nav navbar-nav lpth-navbar">
						<li class=""><a href="index.php">INICIO</a></li>
						<li class=""><a href="retrieve_news.php">NOTICIAS</a></li>
						<li class="active"><a href="activityRetrieve.php">ACTIVIDADES</a></li>
						<li class=""><a href="circuit_descrip.php">CIRCUITOS</a></li>
						<li class=""><a href="glossary.php">GLOSARIO</a></li>
						<li class=""><a href="contact.php">CONTACTO</a></li>
						<li><a>
							<?php
								session_start();
								if(empty($_SESSION['username'])){
									echo "<li><a href='login.php'>[Iniciar Sesión]</a></li>";
								}else{
								echo "
								<div class='btn-group hidden-sm hidden-xs pull-right'>
									</button>
									<ul class='dropdown-menu'>
									    	<li><a href='#'>Perfil</a></li>
								    		<li><a href='#'>Configuraci&oacute;n</a></li>
								    		<li role='separator' class='divider'></li>
								    		<li><a href='#'>Cerrar Sesi&oacute;n</a></li>
								  	</ul>
								</div>
								<div class='pull-right'>
									<li class='login.php'><a href='perfil.php'>[".$_SESSION['username']."]
										<span class='glyphicon glyphicon-user' data-toggle='tooltip' data-placement='bottom' title='Perfil y Configuraci&oacute;n'></span> </a></li>
									</p>
								</div>";
								}
								//else
							?>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<div class="header">
			<a href="index.php"><img src="img/header.png" alt="LPTH"></a>
		</div>
	</header>
	<!--FinNavbar-->
	<!--MainContent-->
	<section class="main container">
		<div class="row">
			<section class="col-md-9 col-lg-9">
				<article class="post page-header clearfix">
					<?php
						echo "<a href='activityRetrieve.php' class='btn btn-default btn-block btn-lg'>Volver a la Lista de Actividades</span></a>";
						$conn = mysqli_connect('localhost','root','','lapapelera');
						mysqli_select_db($conn,'lapapelera');
						mysqli_set_charset($conn,"utf8");

						$id = $_REQUEST['id'];
						$query = "SELECT * FROM actividad WHERE idActividad='".$id."';";
						$result = mysqli_query($conn,$query);

						while($row = mysqli_fetch_array($result)){
							//Mostrar c/Actividad
							echo "<article class='post page-header clearfix'>";
							/*Imagen*/
							echo "<a href='getact.php?id=".$row['idActividad']."' class='thumb pull-left'><img class='img-thumbnail' src=".$row['imagen']." alt='Imagen de Actividad'></a>";
							/*Titulo*/
							echo "<h2 class='post-title'><a href='getact.php?id=".$row['idActividad']."'>".$row['titulo']."</a></h2>";
							/*fechaInic*/
							echo "<h6 class='post-date text-justify'>Fecha de Inicio: ".$row['startDate']."</h6>";
							/*fechaFin*/
							echo "<h6 class='post-date text-justify'>Fecha de Fin: ".$row['endDate']."</h6>";
							/*Publicado [fecha] por [autor]*/
							echo "<h6><span class='post-date'>Publicado el ".$row['fecha']."</span> por <span><a href='$'>".$row['autor']."</a></span></h6>";
							echo"<br>";
							/*Contenido*/
							echo "<p class='post-content text-justify'>".$row['descripcion']."</p>";
							echo "<div class='container-buttons'>";
							echo "</div>";
							echo "</article>";
						}//while
						//listComments
						$query = "SELECT * FROM comentario where idActividad = ".$id." ORDER BY idComentario DESC";
						$result = mysqli_query($conn,$query);
						while($row = mysqli_fetch_array($result)){
							//Mostrar c/Noticia
							echo "<article class='post page-header clearfix'>";
							/*Publicado [fecha] por [autor]*/
							echo "<p><span class='post-date'>Comentario de <a href='perfil.php?idUsuario=".$row['autor']."'>".$row['autor']."</a> el ".$row['fecha']."</p>";
							/*Contenido*/
							echo "<p class='post-content text-justify'><blockquote>".$row['contenido']."</blockquote></p>";
						}//while--ListComments
					?>
					<!--comments-->
					<hr id="comments">
					<fieldset>
						<form action="commentManager.php" method="post">
							<label for="content"><b>Comentando como:</b></label>
							<?php
								if(!empty($_SESSION['username']))
								{
									echo "<input type='text' disabled name='autor' id='autor' required minlength='1' value=".$_SESSION['username'].">";
									date_default_timezone_get();
									$current = date('Y-m-d');
									echo "<input type='date' hidden name='fecha' id='fecha' value=".$current.">
									<input type='text' hidden name='idNoticia' id='idNoticia' value=".$id.">";
									//aclarar si pertenece a new o act:
									echo "<input type='text' hidden name='new_or_act' id='new_or_act' value='act'>";
								}else{
									echo "No tienes permisos para publicar.";
								}
							?>
							<br>
							<label for="content"><b>Contenido</b></label>
							<br>
							<textarea name="content" id="content" value="" rows="6" cols="103" maxlength="1000" placeholder=" Ingresa tu comentario aquí." required></textarea>
							<?php
								if(!empty($_SESSION['username']))
								{
									echo "<input class='btn btn-primary btn-block btn-lg' type='submit' id='btnPublicar' name='btnPublicar' value='Publicar Comentario'>";
								}else{
									echo "<br><span class='pull-right'>No tienes permisos para publicar.</span>";
								}
							?>
							</form><!--/comments-->
							</fieldset>
						</article>
					</section> 
					<!--Sidebar-->
					<aside class="col-md-3 col-lg-3 hidden-xs hidden-sm pull-right">
						<h4>Usuarios</h4>
						<div class="list-group">
							<?php
							if(empty($_SESSION['username'])){
								echo "<a class='list-group-item' href='login.php' class='list-group-item'>Iniciar Sesion</a>";
								echo "<a class='list-group-item' href='signup.php' class='list-group-item'>Crear Usuario</a>";
							}else{
								echo "<a href='perfil.php' class='list-group-item'>Mi Perfil</a>";
								echo "<a href='reportes.php' class='list-group-item'>Estadísticas</a>";
								echo "<a href='destroy.php' class='list-group-item'>Salir</a>";
							}
							?>
				</div>
				<h4>Actividades</h4>
				<div class="list-group">
					<a class="list-group-item" href="#">Más populares</a>
					<a class="list-group-item" href="#">En tu zona</a>
					<a class="list-group-item" href="activityRetrieve.php">Más recientes</a>
					<br>
					<a class="list-group-item" href="zonanorte.php">Zona Norte</a>
					<a class="list-group-item" href="zonasur.php">Zona Sur</a>
				</div>
				<h4>Contacto</h4>
				<h4>Soporte Telefónico</h4>
				<p>(0261) 7778698<p/>
				<p>(0212) 7546939</p>
				<h4>Correo Electrónico</h4>
				<p>soporte@lapapelera.com</p>
			</aside>
			<!--FinSidebar-->
		</div>
	</section>
	<?php include "pieces/footer.php" ?>
	<script src="js/jquery-3.1.1.min.js"></script>
	<script src="js/bootstrap.js"></script>
</body>
</html>