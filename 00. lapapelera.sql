﻿--
DROP DATABASE IF EXISTS lapapelera;
CREATE DATABASE lapapelera;
USE lapapelera;

CREATE TABLE categoria(
etiqueta VARCHAR(30) NOT NULL PRIMARY KEY);
/*{ACA PODRIA SER TIPO ENUM MAYBE?}*/

CREATE TABLE circuito(
idCircuito INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
nomCirc VARCHAR(30) NOT NULL,
ubic_sede VARCHAR(500) NOT NULL,
imagen VARCHAR(2000),
descripcion VARCHAR(500));

CREATE TABLE usuario(
username VARCHAR(30) NOT NULL PRIMARY KEY,
pword VARCHAR(30) NOT NULL,
nombre VARCHAR(30) NOT NULL,
apellido VARCHAR(30) NOT NULL,
email VARCHAR(30) NOT NULL,
telef VARCHAR(11),
edad INT,
direccion VARCHAR(100),
userType ENUM('administrador','lider','usuario') NOT NULL DEFAULT "usuario",
idCircuito INT,
FOREIGN KEY(idCircuito) references circuito(idCircuito)
ON UPDATE cascade 
ON DELETE set null);

CREATE TABLE noticia(
idNoticia INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
titulo VARCHAR(50) NOT NULL DEFAULT "Noticia",
contenido VARCHAR(5000) NOT NULL DEFAULT "-",
imagen VARCHAR(200) NOT NULL DEFAULT "multimedia/default.png",
fecha DATE NOT NULL DEFAULT "1999-01-01",
autor VARCHAR(30),
etiqueta VARCHAR(30),									
FOREIGN KEY(autor) REFERENCES usuario(username)		
ON UPDATE cascade 									
ON DELETE set null,
FOREIGN KEY(etiqueta) REFERENCES categoria(etiqueta)
ON UPDATE cascade 
ON DELETE set null);								

CREATE TABLE actividad( 
idActividad INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
titulo VARCHAR(50) NOT NULL,
descripcion VARCHAR(500) NOT NULL,
fecha DATE NOT NULL,
startDate DATE NOT NULL,
endDate DATE NOT NULL,
imagen VARCHAR(200) NOT NULL DEFAULT "multimedia/default.png",
autor VARCHAR(30),
etiqueta VARCHAR(30),
idCircuito INT,
FOREIGN KEY(idCircuito) references circuito(idCircuito)	
ON UPDATE cascade 									
ON DELETE set null,								
FOREIGN KEY(autor) references usuario(username)	
ON UPDATE cascade 									
ON DELETE set null,	
FOREIGN KEY(etiqueta) references categoria(etiqueta)
ON UPDATE cascade 
ON DELETE set null);								

CREATE TABLE comentario(
idComentario INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
fecha DATE NOT NULL,
contenido VARCHAR(1000),
autor VARCHAR(30),	
idNoticia INT,		
idActividad INT,
FOREIGN KEY(idNoticia) references noticia(idNoticia)	
ON UPDATE cascade 									
ON DELETE set null,	
FOREIGN KEY(idActividad) references actividad(idActividad)	
ON UPDATE cascade 									
ON DELETE set null,								
FOREIGN KEY(autor) references usuario(username)	
ON UPDATE cascade 									
ON DELETE set null);							

INSERT INTO circuito(idCircuito,nomCirc,ubic_sede,descripcion)
VALUES(null,"Zona Sur","Palacio de combate","Circuito para la zona Sur de maracaibo"),(null,"Zona Norte","Urbe ","Circuito para la zona Norte de maracaibo");

INSERT INTO usuario(username,pword,nombre,apellido,email,userType,idCircuito)
VALUES("admin","111111","Alejandro","Lopez","alejandrolopez.gd@gmail.com","administrador",1),("lider","111111","Luis","Labarca","luismiguelab@gmail.com","lider",2),("user1","111111","Gerardo","Sánchez","gerardosb95@gmail.com","usuario",1);

INSERT INTO noticia(titulo,contenido,imagen,fecha,autor)
VALUES
("¡Trabaja para Greenpeace!","La organización no gubernamental (ONG) ecológica Greenpeace International ha lanzado diversas ofertas de empleo para profesionales, así como para integrar su programa de prácticas remuneradas. Fundada en 1971 en Canadá, la entidad se dedica a la protección del medio ambiente a través de diversas acciones y campañas de concienciación en distintos puntos del planeta. Te acercamos los nuevos llamados de trabajo y prácticas. <b>Fuente</b>: Greenpeace.","multimedia/trabajar-greenpeace-international.jpg","2000-01-1","admin"),
("Organización Zuliana «La Papelera Tiene Hambre»",'El movimiento ecológico La Papelera Tiene Hambre, a través de su programa Vecino Sustentable, invita a donar los sábados de cada mes los residuos domésticos de forma clasificada (papel, plástico, aluminio/metal, basura electrónica).<br><br>Bajo el lema de: «Una ciudad limpia no es la que más se barre, sino la que menos se ensucia» arrancó en el año 2012 el movimiento ecológico llamado «La Papelera tiene Hambre» que cuenta con un equipo humano que posee grandes proyectos para tratar de que la ciudad y el país sean espacios donde las personas sean conscientes de que pueden lograr cambios para detener el problema ambiental que aqueja en la actualidad. El reciclaje es una contribución importante en la disminución de las cantidades de residuos que deben eliminarse en vertederos, por incineración o por otras vías. Con esta acción se promueve el pensamiento ecologista en la población. Al reciclar se realza la responsabilidad por preservar los recursos naturales. Antonio Soto “En estos encuentros le hacemos saber a la población la necesidad de concienciar a las personas “ Antonio Soto, organizador de este movimiento ecológico, resaltó que el objetivo general es «educar a las personas con campañas creativas en la ciudad sobre la importancia que tiene el sentido de pertenencia del medio ambiente, queremos que las personas sepan lo importante que son estas acciones».<br><br>Ciudadela Faría Soto destacó que comenzaron esta iniciativa en la urbanización Ciudadela Faría porque ya varias organizaciones no gubernamentales (ONG) como Azul Ambientalista, la Universidad del Zulia y el Ministerio de Ambiente han estado presentes por ser una comunidad que se ha visto afectada por la inseguridad e invasiones. Además resaltó que cuentan con la colaboración de todos los vecinos de la Urbanización Ciudadela Faría y de estudiantes de la universidad Rafael Belloso Chacín (URBE), quienes realizan el servicio comunitario en el sector, pintando murales alusivos al tema. Informó que como organización su principal idea es «clasificar todos los desechos y venderlos para obtener ingresos que son utilizados en mejorar las áreas verdes de la urbanización, sembrar árboles, colocar sistemas de riego, todo esto es con el dinero que obtenemos por el reciclaje», destaca. Sábados de actividades El primer sábado de cada mes es el día de reunirse en la urbanización en la calle 59A al lado del centro comercial Ciudadela Plaza, donde se concentran todos los vecinos que apoyan este tipo de actividades que van en pro de un mejor estilo de vida y lograr contar con áreas verdes dignas para el disfrute de todos. El creador de este grupo señaló que: «En estos encuentros le hacemos saber a la población la necesidad de concienciar a las personas que botan la basura al suelo, por la ventana del carro, al lago, las playas, parques, plazas o en cualquier espacio público de nuestra ciudad. Queremos y necesitamos una ciudad limpia».<br><br><b>Vecinos Unidos</b> En varias oportunidades han realizado actividades deportivas, recreativas, pintan murales, realizan feria de verduras y hortalizas en la cancha de la comunidad con el fin de obtener el mejor beneficio de estas acciones. «Los vecinos han tenido buena receptividad y estas personas han logrado involucrar a muchas más, algunas han asumido el cuidado de algunas áreas, otros se han activado con formas creativas de comercio individual ofreciendo sus productos o sus servicios, todas estas actividades hacen de esta cancha, un lugar seguro para el sector», apuntó Soto. Beneficios del reciclaje Luego de separar y lavar los desechos —botellas, potes, etc— se pueden aprovechar en la comunidad, si se establecen centros de acopio o almacenamiento para luego comercializarlos a través de las diferentes organizaciones o empresas recicladoras. Asimismo, algunos productos recuperados tales como tablas de madera, cartones de huevos y rollos de papel sanitario entre otros, sirven para la fabricación de objetos artesanales. La materia orgánica o desechos de alimentos puede utilizarse para la preparación de restauradores del suelo o abono —material húmico— por sus características y las diferentes sustancias que contiene. Además, separar y acopiar los desechos puede generar múltiples beneficios económicos, aparte de los relacionados con la conservación del ambiente, los cuales pueden servir para el financiamiento de servicios municipales como agua, luz, aseo urbano o el financiamiento de pequeñas obras comunales o actividades sociales.<br><br><b>Basura electrónica</b> Los aparatos electrónicos van avanzando con el pasar del tiempo y la población no sabe qué hacer con los aparatos obsoletos. La buena noticia es que hoy existen organizaciones que se encargan de que todos los desechos electrónicos no sean tratados como tal, sino que puedan ser aprovechados de forma eficiente.',"multimedia/activ2.jpg",'2016-10-25','admin');

INSERT INTO actividad(titulo,descripcion,fecha,startDate,endDate,imagen,autor,idCircuito)
VALUES
('Actividad: “Sembrando Planeta”','Actividades ecológicas, fomentando el sentido de pertenencia, valorar la naturaleza y cómo ser un agente de cambio. Se visitarán colegios para la realización de charlas, presentaciones de la importancia que tiene conservar el planeta, la naturaleza, tus espacios. Cómo ser un agente de cambio en la sociedad y realizar actividades para aprender a reciclar en casa. De igual forma se busca que los colegios y profesores, adhieran estas actividades dentro de sus programas educativos basándose en la nueva generación como propulsores de cambios. ¿Quieres que visitemos tu escuela o empresa? Escríbenos al correo lapapeleratienehambre@gmail.com (válido para la Zona Norte de Maracaibo).','2016-10-31','2016-10-31','2016-11-04','multimedia/sembrandoplaneta.jpg','admin',1),
('Actividad: “Vecino Sustentable”','El movimiento ecológico La Papelera tiene hambre, a través de su programa Vecino Sustentable, invita a donar los sábados de cada mes los residuos domésticos de forma clasificada (papel, plástico, aluminio/metal, basura electrónica). Este sábado 12 de Noviembre podrás colaborar con esta causa que ayudará a mantener una ciudad más limpia y ecológica. La urbanización Ciudadela Faría, ubicada al noroeste de Maracaibo será el centro de acopio de residuos Punto A: Frente a Residencias La Puerta, diagonal al C.C. Ciudadela Plaza. Calle 59A. Punto B: Centro comercial Andreína, frente a la farmacia Saas. Hora: a partir de las 8:00 am hasta las 12:00 pm','2016-10-31','2016-11-12','2016-11-12','multimedia/timthumb.jpg','admin',1);